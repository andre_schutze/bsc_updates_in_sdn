# Copyright (C) 2012 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re, os

import logging

import json, time, random
import ast
from webob import Response

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller import dpset
from ryu.controller.handler import MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_0
from ryu.ofproto import ofproto_v1_2
from ryu.ofproto import ofproto_v1_3
from ryu.lib import ofctl_v1_0
from ryu.lib import ofctl_v1_2
from ryu.lib import ofctl_v1_3
from ryu.app.wsgi import ControllerBase, WSGIApplication


LOG = logging.getLogger('ryu.app.ofctl_rest')

# supported ofctl versions in this restful app
supported_ofctl = {
    ofproto_v1_0.OFP_VERSION: ofctl_v1_0,
    ofproto_v1_2.OFP_VERSION: ofctl_v1_2,
    ofproto_v1_3.OFP_VERSION: ofctl_v1_3,
}

# REST API
#

# Retrieve the switch stats
#
# get the list of all switches
# GET /stats/switches
#
# get the desc stats of the switch
# GET /stats/desc/<dpid>
#
# get flows stats of the switch
# GET /stats/flow/<dpid>
#
# get flows stats of the switch filtered by the fields
# POST /stats/flow/<dpid>
#
# get aggregate flows stats of the switch
# GET /stats/aggregateflow/<dpid>
#
# get aggregate flows stats of the switch filtered by the fields
# POST /stats/aggregateflow/<dpid>
#
# get ports stats of the switch
# GET /stats/port/<dpid>
#
# get queues stats of the switch
# GET /stats/queue/<dpid>
#
# get meter features stats of the switch
# GET /stats/meterfeatures/<dpid>
#
# get meter config stats of the switch
# GET /stats/meterconfig/<dpid>
#
# get meters stats of the switch
# GET /stats/meter/<dpid>
#
# get group features stats of the switch
# GET /stats/groupfeatures/<dpid>
#
# get groups desc stats of the switch
# GET /stats/groupdesc/<dpid>
#
# get groups stats of the switch
# GET /stats/group/<dpid>
#
# get ports description of the switch
# GET /stats/portdesc/<dpid>

# Update the switch stats
#
# add a flow entry
# POST /stats/flowentry/add
#
# modify all matching flow entries
# POST /stats/flowentry/modify
#
# modify flow entry strictly matching wildcards and priority
# POST /stats/flowentry/modify_strict
#
# delete all matching flow entries
# POST /stats/flowentry/delete
#
# delete flow entry strictly matching wildcards and priority
# POST /stats/flowentry/delete_strict
#
# delete all flow entries of the switch
# DELETE /stats/flowentry/clear/<dpid>
#
# add a meter entry
# POST /stats/meterentry/add
#
# modify a meter entry
# POST /stats/meterentry/modify
#
# delete a meter entry
# POST /stats/meterentry/delete
#
# add a group entry
# POST /stats/groupentry/add
#
# modify a group entry
# POST /stats/groupentry/modify
#
# delete a group entry
# POST /stats/groupentry/delete
#
# modify behavior of the physical port
# POST /stats/portdesc/modify
#
#
# send a experimeter message
# POST /stats/experimenter/<dpid>

print(os.getcwd())
timefile = open('timefile.txt', 'w')

from datetime import datetime


class OwnSheduleRandomDelayMessage(object):

    def __init__(self, first, second, third, fourth, fifth, flowentries, dpset, statsController, pRoundTime):
	self.first = []
	self.second = []
	self.third = []
	self.fourth = []
	self.fifth = []
	self.barrier = []
	self.flowentries = []
	self.dpset = {}
	self.statsController = None
	self.inprogress = False
	self.step = 0
	self.roundTime = float(pRoundTime)
	for item in first:
	    self.first.append(int(item))

        for item in second:
	    self.second.append(int(item))

        for item in third:
	    self.third.append(int(item))

        for item in fourth:
	    self.fourth.append(int(item))

	for item in fifth:
	    self.fifth.append(int(item))

	LOG.debug('first round: %s', self.first)
        LOG.debug('second round: %s', self.second)
        LOG.debug('third round: %s', self.third)
        LOG.debug('fourth round: %s', self.fourth)
	LOG.debug('fifth round: %s', self.fifth)

	self.flowentries = flowentries
        if len(first) > 0:
            self.step = 1
        elif len(second) > 0:
            self.step = 2
        elif len(third) > 0:
            self.step = 3
        elif len(fourth) > 0:
            self.step = 4
	elif len(fifth) > 0:
	    self.step = 5

	self.dpset = dpset
        self.statsController = statsController

    def getInProgress(self):
        return self.inprogress

    def setInProgress(self):
        self.inprogress = True

    def addReply(self, dpid):
        LOG.debug('items in barrier %s', self.barrier)
        if int(dpid) in self.barrier:
            self.barrier.remove(dpid)
            if len(self.barrier) == 0:
                if self.step == 1:
                    if len(self.second) > 0:
                        self.step = 2
                    elif len(self.third) > 0:
                        self.step = 3
                    elif len(self.fourth) > 0:
                        self.step = 4
		    elif len(self.fifth) > 0:
			self.step = 5
                    else:
                        self.step = 6

                elif self.step == 2:
                    if len(self.third) > 0:
                        self.step = 3
                    elif len(self.fourth) > 0:
                        self.step = 4
		    elif len(self.fifth) > 0:
			self.step = 5
                    else:
                        self.step = 6

                elif self.step == 3:
                    if len(self.fourth) > 0:
                        self.step = 4
		    elif len(self.fifth) > 0:
			self.step = 5
                    else:
                        self.step = 6

                elif self.step == 4:
		    if len(self.fifth) > 0:
			self.step = 5
		    else:
			self.step = 6

		elif self.step == 5:
		    self.step = 6

            return True
        else:
            return False

    def getRound(self):
        return self.step

    def getNextRoundFlowEntries(self):
        if self.step == 1:
            if len(self.first) > 0:
                return self.getFirstFlowEntries()
            elif len(self.second) > 0:
                self.step = 2
                return self.getSecondFlowEntries()
            elif len(self.third) > 0:
                self.step = 3
                return self.getThirdFlowEntries()
            elif len(self.fourth) > 0:
                self.step = 4
                return self.getFourthFlowEntries()
	    elif len(self.fifth) > 0:
		self.step = 5
		return self.getFifthFlowEntries()
            else:
                return None
        elif self.step == 2:
            if len(self.second) > 0:
                return self.getSecondFlowEntries()
            elif len(self.third) > 0:
                self.step = 3
                return self.getThirdFlowEntries()
            elif len(self.fourth) > 0:
                self.step = 4
                return self.getFourthFlowEntries()
	    elif len(self.fifth) > 0:
		self.step = 5
		return self.getFifthFlowEntries()
            else:
                return None
        elif self.step == 3:
            if len(self.third) > 0:
                return self.getThirdFlowEntries()
            elif len(self.fourth) > 0:
		self.step = 4
                return self.getFourthFlowEntries()
	    elif len(self.fifth) > 0:
		self.step = 5
		return self.getFifthFlowEntries()
            else:
                return None
        elif self.step == 4:
            if len(self.fourth) > 0:
                return self.getFourthFlowEntries()
	    elif len(self.fifth) > 0:
		self.step = 5
		return self.getFifthFlowEntries()
            else:
                return None

	elif self.step == 5:
	    if len(self.fifth) > 0:
		return self.getFifthFlowEntries()
	    else:
		return None

	elif self.step == 6:
	    return None

    def getFirstFlowEntries(self):
        first_flowentries = []
        for i in range(0,len(self.flowentries)):
            flowentry = self.flowentries[i]
            key = flowentry.keys()[0]
            msg_type = key.split(',')[0]
            dpid = key.split(',')[1]
            if int(dpid) in self.first:
                first_flowentries.append(flowentry)
        return first_flowentries

    def getSecondFlowEntries(self):
        second_flowentries = []
        for i in range(0,len(self.flowentries)):
            flowentry = self.flowentries[i]
            key = flowentry.keys()[0]
            msg_type = key.split(',')[0]
            dpid = key.split(',')[1]
            if int(dpid) in self.second:
                second_flowentries.append(flowentry)
        return second_flowentries

    def getThirdFlowEntries(self):
        third_flowentries = []
        for i in range(0,len(self.flowentries)):
            flowentry = self.flowentries[i]
            key = flowentry.keys()[0]
            msg_type = key.split(',')[0]
            dpid = key.split(',')[1]
            if int(dpid) in self.third:
                third_flowentries.append(flowentry)
        return third_flowentries

    def getFourthFlowEntries(self):
        fourth_flowentries = []
        for i in range(0,len(self.flowentries)):
            flowentry = self.flowentries[i]
            key = flowentry.keys()[0]
            msg_type = key.split(',')[0]
            dpid = key.split(',')[1]
            if int(dpid) in self.fourth:
                fourth_flowentries.append(flowentry)
        return fourth_flowentries

    def getFifthFlowEntries(self):
        fifth_flowentries = []
        for i in range(0,len(self.flowentries)):
            flowentry = self.flowentries[i]
            key = flowentry.keys()[0]
            msg_type = key.split(',')[0]
            dpid = key.split(',')[1]
            if int(dpid) in self.fifth:
                fifth_flowentries.append(flowentry)
        return fifth_flowentries

    def send_next_round(self):
        LOG.debug('send next round messages')
        next_round_flowentries = self.getNextRoundFlowEntries()

        if next_round_flowentries != None:
            LOG.debug('%s messages to be send in this round', len(next_round_flowentries))
	    self.send_round(next_round_flowentries)

    def send_round(self, flowentries):
#	roundTime = 1
	dpidTimes = {}
	for flowentry in flowentries:
	    key = flowentry.keys()[0]
	    dpid = key.split(',')[1]
	    timeDpid = random.random() * self.roundTime
	    dpidTimes[int(dpid)] = timeDpid

	dpidTimesCloned = {}
	for key in dpidTimes:
	    dpidTimesCloned[key] = dpidTimes[key]

	dpidSorted = []
	while len(dpidTimesCloned) != 0:
	    smallestDpid = dpidTimesCloned.keys()[0]
	    smallestTime = dpidTimesCloned[smallestDpid]
	    for key in dpidTimesCloned:
		if dpidTimesCloned[key] < smallestTime:
		    smallestDpid = key
		    smallestTime = dpidTimesCloned[key]
	    dpidSorted.append(smallestDpid)
	    del dpidTimesCloned[smallestDpid]

	previousTime = 0.0
	for dpid in dpidSorted:
	    diff = dpidTimes[dpid] - previousTime
	    time.sleep(diff)
	    previousTime = dpidTimes[dpid]
	    for flowentry in flowentries:
		key = flowentry.keys()[0]
		dpidFlowentry = key.split(',')[1]
		if int(dpidFlowentry) == dpid:
		    cmd = key.split(',')[0]
		    message = flowentry[key]
		    if dpid not in self.barrier:
			self.barrier.append(dpid)
		    LOG.debug("send %s message at %f", cmd, dpidTimes[dpid])
		    self.statsController.send_flow_mod_message(cmd, message)

	diff = self.roundTime - previousTime
	time.sleep(diff)

	for i in self.barrier:
	    self.send_barrier_request(i)


    def send_barrier_request(self, dpid):
        LOG.debug('send barrier request to %s', dpid)
        dp = self.dpset.get(dpid)
        ofp_parser = dp.ofproto_parser
        req = ofp_parser.OFPBarrierRequest(dp)
        dp.send_msg(req)

class OwnSheduleMessage(object):

    def __init__(self, first, second, third, fourth, fifth, flowentries, dpset, statsController):
	self.first = []
	self.second = []
	self.third = []
	self.fourth = []
	self.fifth = []
	self.barrier = []
	self.flowentries = []
	self.dpset = {}
	self.statsController = None
	self.inprogress = False
	self.step = 0
	for item in first:
	    self.first.append(int(item))

        for item in second:
	    self.second.append(int(item))

        for item in third:
	    self.third.append(int(item))

        for item in fourth:
	    self.fourth.append(int(item))

	for item in fifth:
	    self.fifth.append(int(item))

	LOG.debug('first round: %s', self.first)
        LOG.debug('second round: %s', self.second)
        LOG.debug('third round: %s', self.third)
        LOG.debug('fourth round: %s', self.fourth)
	LOG.debug('fifth round: %s', self.fifth)

	self.flowentries = flowentries
        if len(first) > 0:
            self.step = 1
        elif len(second) > 0:
            self.step = 2
        elif len(third) > 0:
            self.step = 3
        elif len(fourth) > 0:
            self.step = 4
	elif len(fifth) > 0:
	    self.step = 5

	self.dpset = dpset
        self.statsController = statsController

    def getInProgress(self):
        return self.inprogress

    def setInProgress(self):
        self.inprogress = True

    def addReply(self, dpid):
        LOG.debug('items in barrier %s', self.barrier)
        if int(dpid) in self.barrier:
            self.barrier.remove(dpid)
            if len(self.barrier) == 0:
                if self.step == 1:
                    if len(self.second) > 0:
                        self.step = 2
                    elif len(self.third) > 0:
                        self.step = 3
                    elif len(self.fourth) > 0:
                        self.step = 4
		    elif len(self.fifth) > 0:
			self.step = 5
                    else:
                        self.step = 6

                elif self.step == 2:
                    if len(self.third) > 0:
                        self.step = 3
                    elif len(self.fourth) > 0:
                        self.step = 4
		    elif len(self.fifth) > 0:
			self.step = 5
                    else:
                        self.step = 6

                elif self.step == 3:
                    if len(self.fourth) > 0:
                        self.step = 4
		    elif len(self.fifth) > 0:
			self.step = 5
                    else:
                        self.step = 6

                elif self.step == 4:
		    if len(self.fifth) > 0:
			self.step = 5
		    else:
			self.step = 6

		elif self.step == 5:
		    self.step = 6

            return True
        else:
            return False

    def getRound(self):
        return self.step

    def getNextRoundFlowEntries(self):
        if self.step == 1:
            if len(self.first) > 0:
                return self.getFirstFlowEntries()
            elif len(self.second) > 0:
                self.step = 2
                return self.getSecondFlowEntries()
            elif len(self.third) > 0:
                self.step = 3
                return self.getThirdFlowEntries()
            elif len(self.fourth) > 0:
                self.step = 4
                return self.getFourthFlowEntries()
	    elif len(self.fifth) > 0:
		self.step = 5
		return self.getFifthFlowEntries()
            else:
                return None
        elif self.step == 2:
            if len(self.second) > 0:
                return self.getSecondFlowEntries()
            elif len(self.third) > 0:
                self.step = 3
                return self.getThirdFlowEntries()
            elif len(self.fourth) > 0:
                self.step = 4
                return self.getFourthFlowEntries()
	    elif len(self.fifth) > 0:
		self.step = 5
		return self.getFifthFlowEntries()
            else:
                return None
        elif self.step == 3:
            if len(self.third) > 0:
                return self.getThirdFlowEntries()
            elif len(self.fourth) > 0:
		self.step = 4
                return self.getFourthFlowEntries()
	    elif len(self.fifth) > 0:
		self.step = 5
		return self.getFifthFlowEntries()
            else:
                return None
        elif self.step == 4:
            if len(self.fourth) > 0:
                return self.getFourthFlowEntries()
	    elif len(self.fifth) > 0:
		self.step = 5
		return self.getFifthFlowEntries()
            else:
                return None

	elif self.step == 5:
	    if len(self.fifth) > 0:
		return self.getFifthFlowEntries()
	    else:
		return None

	elif self.step == 6:
	    return None

    def getFirstFlowEntries(self):
        first_flowentries = []
        for i in range(0,len(self.flowentries)):
            flowentry = self.flowentries[i]
            key = flowentry.keys()[0]
            msg_type = key.split(',')[0]
            dpid = key.split(',')[1]
            if int(dpid) in self.first:
                first_flowentries.append(flowentry)
        return first_flowentries

    def getSecondFlowEntries(self):
        second_flowentries = []
        for i in range(0,len(self.flowentries)):
            flowentry = self.flowentries[i]
            key = flowentry.keys()[0]
            msg_type = key.split(',')[0]
            dpid = key.split(',')[1]
            if int(dpid) in self.second:
                second_flowentries.append(flowentry)
        return second_flowentries

    def getThirdFlowEntries(self):
        third_flowentries = []
        for i in range(0,len(self.flowentries)):
            flowentry = self.flowentries[i]
            key = flowentry.keys()[0]
            msg_type = key.split(',')[0]
            dpid = key.split(',')[1]
            if int(dpid) in self.third:
                third_flowentries.append(flowentry)
        return third_flowentries

    def getFourthFlowEntries(self):
        fourth_flowentries = []
        for i in range(0,len(self.flowentries)):
            flowentry = self.flowentries[i]
            key = flowentry.keys()[0]
            msg_type = key.split(',')[0]
            dpid = key.split(',')[1]
            if int(dpid) in self.fourth:
                fourth_flowentries.append(flowentry)
        return fourth_flowentries

    def getFifthFlowEntries(self):
        fifth_flowentries = []
        for i in range(0,len(self.flowentries)):
            flowentry = self.flowentries[i]
            key = flowentry.keys()[0]
            msg_type = key.split(',')[0]
            dpid = key.split(',')[1]
            if int(dpid) in self.fifth:
                fifth_flowentries.append(flowentry)
        return fifth_flowentries

    def send_next_round(self):
        LOG.debug('send next round messages')
        next_round_flowentries = self.getNextRoundFlowEntries()

        if next_round_flowentries != None:
            LOG.debug('%s messages to be send in this round', len(next_round_flowentries))

	    if self.step != 2:
		for flowentry in next_round_flowentries:
		    key = flowentry.keys()[0]
                    message = flowentry[key]
                    cmd = key.split(',')[0]
                    dpid = key.split(',')[1]
                    if int(dpid) not in self.barrier:
                	self.barrier.append(int(dpid))
                    LOG.debug("send %s message", cmd)
                    self.statsController.send_flow_mod_message(cmd, message)

#                LOG.debug('start waiting for 1 second')
#                time.sleep(1)
#                LOG.debug('waiting finished')

                for i in self.barrier:
                    self.send_barrier_request(i)
		
	    elif self.step == 2:
		for flowentry in next_round_flowentries:
                    key = flowentry.keys()[0]
                    message = flowentry[key]
                    cmd = key.split(',')[0]
                    dpid = key.split(',')[1]
		    if int(dpid) == 16:
                        if int(dpid) not in self.barrier:
                            self.barrier.append(int(dpid))
                        LOG.debug("send %s message", cmd)
                        self.statsController.send_flow_mod_message(cmd, message)
			time.sleep(1)


                for flowentry in next_round_flowentries:
                    key = flowentry.keys()[0]
                    message = flowentry[key]
                    cmd = key.split(',')[0]
                    dpid = key.split(',')[1]
                    if int(dpid) != 16:
                        if int(dpid) not in self.barrier:
                            self.barrier.append(int(dpid))
                        LOG.debug("send %s message", cmd)
                        self.statsController.send_flow_mod_message(cmd, message)

                for i in self.barrier:
                    self.send_barrier_request(i)

    def send_barrier_request(self, dpid):
        LOG.debug('send barrier request to %s', dpid)
        dp = self.dpset.get(dpid)
        ofp_parser = dp.ofproto_parser
        req = ofp_parser.OFPBarrierRequest(dp)
        dp.send_msg(req)



class WayUpMessage(object):
#    first = []
#    second = []
#    third = []
#    fourth = []
#    interval = 0
#    barrier = []
#    flowentries = []
#    dpset = {}
#    statsController = None
#    inprogress = False
#    step = 0

    def __init__(self, first, second, third, fourth, interval, flowentries, dpset, statsController):
	self.first = []
	self.second = []
	self.third = []
	self.fourth = []
	self.interval = 0
	self.barrier = []
	self.flowentries = []
	self.dpset = {}
	self.statsController = None
	self.inprogress = False
	self.step = 0
	for item in first:
	    if item.isdigit():
		self.first.append(int(item))
	
	for item in second:
	    if item.isdigit():
		self.second.append(int(item))
	
	for item in third:
	    if item.isdigit():
		self.third.append(int(item))

	for item in fourth:
	    if item.isdigit():
		self.fourth.append(int(item))
	print(interval)
	self.interval = int(interval)/1000

	LOG.debug('first round: %s', self.first)
	LOG.debug('second round: %s', self.second)
	LOG.debug('third round: %s', self.third)
	LOG.debug('fourth round: %s', self.fourth)
	
	self.flowentries = flowentries
	if len(first) > 0:
	    self.step = 1
	#    self.barrier = self.first
	elif len(second) > 0:
	    self.step = 2
	#    self.barrier = self.second
	elif len(third) > 0:
	    self.step = 3
	#    self.barrier = self.third
	elif len(fourth) > 0:
	    self.step = 4
	#    self.barrier = self.fourth

	self.dpset = dpset
	self.statsController = statsController

    def getDataPath(self, dpid):
	return self.dpset.get(int(dpid))

    def getInProgress(self):
	return self.inprogress

    def setInProgress(self):
	self.inprogress = True

    def addReply(self, dpid):
	LOG.debug('items in barrier %s', self.barrier)
	if int(dpid) in self.barrier:
	    self.barrier.remove(dpid)
	    if len(self.barrier) == 0:
		if self.step == 1:
		    if len(self.second) > 0:
			self.step = 2
	#		self.barrier = self.second
		    elif len(self.third) > 0:
			self.step = 3
	#		self.barrier = self.third
		    elif len(self.fourth) > 0:
			time.sleep(self.interval)
			self.step = 4
	#		self.barrier = self.fourth
		    else:
			self.step = 5
	#		self.barrier = None

		elif self.step == 2:
		    if len(self.third) > 0:
			self.step = 3
	#		self.barrier = self.third
		    elif len(self.fourth) > 0:
			time.sleep(self.interval)
			self.step = 4
	#		self.barrier = self.fourth
		    else:
			self.step = 5
	#		self.barrier = None

		elif self.step == 3:
		    if len(self.fourth) > 0:
			time.sleep(self.interval)
			self.step = 4
	#		self.barrier = self.fourth
		    else:
			self.step = 5
	#		self.barrier = None

		elif self.step == 4:
		    self.step = 5
	#	    self.barrier = None

	    return True
	else:
	    return False

    def getRound(self):
	return self.step

    def getNextRoundFlowEntries(self):

        now = datetime.now()
        timefile.write("WayUp message send next round: " + str(now) + "\n")
        timefile.flush()



	if self.step == 1:
	    if len(self.first) > 0:
		return self.getFirstFlowEntries()
	    elif len(self.second) > 0:
		self.step = 2
		return self.getSecondFlowEntries()
	    elif len(self.third) > 0:
		self.step = 3
		return self.getThirdFlowEntries()
	    elif len(self.fourth) > 0:
		self.step = 4
		return self.getFourthFlowEntries()
	    else:
		return None
	elif self.step == 2:
	    if len(self.second) > 0:
		return self.getSecondFlowEntries()
	    elif len(self.third) > 0:
		self.step = 3
		return self.getThirdFlowEntries()
	    elif len(self.fourth) > 0:
		self.step = 4
		return self.getFourthFlowEntries()
	    else:
		return None
	elif self.step == 3:
	    if len(self.third) > 0:
		return self.getThirdFlowEntries()
	    elif len(self.fourth) > 0:
		return self.getFourthFlowEntries()
	    else:
		return None
	elif self.step == 4:
	    if len(self.fourth) > 0:
		return self.getFourthFlowEntries()
	    else:
		return None

    def getFirstFlowEntries(self):
	first_flowentries = []
	for i in range(0,len(self.flowentries)):
	    flowentry = self.flowentries[i]
	    key = flowentry.keys()[0]
	    msg_type = key.split(',')[0]
	    dpid = key.split(',')[1]
	    if int(dpid) in self.first:
		first_flowentries.append(flowentry)
	return first_flowentries

    def getSecondFlowEntries(self):
	second_flowentries = []
	for i in range(0,len(self.flowentries)):
	    flowentry = self.flowentries[i]
	    key = flowentry.keys()[0]
	    msg_type = key.split(',')[0]
	    dpid = key.split(',')[1]
	    if int(dpid) in self.second:
		second_flowentries.append(flowentry)
	return second_flowentries

    def getThirdFlowEntries(self):
	third_flowentries = []
	for i in range(0,len(self.flowentries)):
	    flowentry = self.flowentries[i]
	    key = flowentry.keys()[0]
	    msg_type = key.split(',')[0]
	    dpid = key.split(',')[1]
	    if int(dpid) in self.third:
		third_flowentries.append(flowentry)
	return third_flowentries

    def getFourthFlowEntries(self):
	fourth_flowentries = []
	for i in range(0,len(self.flowentries)):
	    flowentry = self.flowentries[i]
	    key = flowentry.keys()[0]
	    msg_type = key.split(',')[0]
	    dpid = key.split(',')[1]
	    if int(dpid) in self.fourth:
		fourth_flowentries.append(flowentry)
	return fourth_flowentries

    def send_next_round(self):
	LOG.debug('send next round messages')
        next_round_flowentries = self.getNextRoundFlowEntries()

        if next_round_flowentries != None:
	    LOG.debug('%s messages to be send in this round', len(next_round_flowentries))
            for flowentry in next_round_flowentries:
                key = flowentry.keys()[0]
                message = flowentry[key]
                cmd = key.split(',')[0]
                dpid = key.split(',')[1]
                if int(dpid) not in self.barrier:
                    self.barrier.append(int(dpid))
		LOG.debug("send %s message", cmd)
                self.statsController.send_flow_mod_message(cmd, message)

            for i in self.barrier:
                self.send_barrier_request(i)

    def send_barrier_request(self, dpid):
        LOG.debug('send barrier request to %s', dpid)
        dp = self.getDataPath(dpid)
        ofp_parser = dp.ofproto_parser
        req = ofp_parser.OFPBarrierRequest(dp)
        dp.send_msg(req)


class MyQueue(object):
#    items = []
    
    def __init__(self):
	self.items = []

    def push(self, item):
	LOG.debug("queue: push item")
	self.items.append(item)

    def pop(self):
	LOG.debug("queue: pop item")
	item = self.items[0]
	self.items.remove(item)
	return item

    def delete(self):
	LOG.debug("queue: delete first item")
	self.items.remove(self.items[0])

    def first(self):
	LOG.debug("queue: get first item")
	if len(self.items) > 0:
	    return self.items[0]
	else:
	    return None

queue = MyQueue()

class PeacockMessage(object):
#    shedule = []
#    flowentries = []
#    dpset = []
#    statsController = None
#    actBarrier = []
#    inProgress = False
#    step = 0

    def __init__(self, shedule, flowentries, dpset, statsController):
	self.shedule = shedule
	self.flowentries = flowentries
	self.dpset = dpset
	self.statsController = statsController
	self.actBarrier = []
	self.inProgress = False
	self.step = 0

    def getRound(self):
	return self.step

    def setInProgress(self):
	self.inProgress = True

    def getInProgress(self):
	return self.inProgress

    def addReply(self, numNode):
	LOG.debug('addReply(%s)', numNode)
	LOG.debug('barrier: %s', self.barrier)
	self.barrier.remove(numNode)
	if len(self.barrier) == 0:
	    if len(self.shedule) == self.step:
		self.inProgress = False
	    else:
	        self.sendNextRoundFlowEntries()

    def getFlowEntries(self, numNode):
	retFlowEntries = []
	for flowEntry in self.flowentries:
	    for key in flowEntry:
		if int(numNode) == int(key.split(',')[1]):
		    retFlowEntries.append(flowEntry)
	return retFlowEntries

    def getDataPath(self, dpid):
        return self.dpset.get(int(dpid))

    def sendNextRoundFlowEntries(self):
	self.step = self.step + 1
	actRound = self.shedule[self.step-1]
	LOG.debug('send round %s with: %s', self.step, actRound)
	for numNode in actRound:
	    LOG.debug('act nodenum: %s', numNode)
	    flowEntries = self.getFlowEntries(numNode)
	    LOG.debug('length of flowentries: %s', len(flowEntries))
	    for flowEntry in flowEntries:
		for key in flowEntry:
		    msgType = key.split(',')[0]
		    dpid = key.split(',')[1]
		    self.statsController.send_flow_mod_message(msgType, flowEntry[key])

	self.barrier = []
	for item in actRound:
	    self.barrier.append(int(item))

	for numNode in self.barrier:
	    self.send_barrier_request(numNode)

    def send_barrier_request(self, dpid):
        LOG.debug('send barrier request to %s', dpid)
        dp = self.getDataPath(dpid)
        ofp_parser = dp.ofproto_parser
        req = ofp_parser.OFPBarrierRequest(dp)
        dp.send_msg(req)

class PeacockPath(object):
#    first = None

    def __init__(self, par1):
#	LOG.debug('initialize PeacockPath %s', par1)
	self.first = None
	if isinstance(par1, list):
#	    LOG.debug('initialize with list')
	    lastElement = None
	    for elem in par1:
#		LOG.debug('node with %s is created', elem)
		newNode = PeacockNode(elem)
		newNode.setMainNum(elem)
		if self.first == None:
		    self.first = newNode
		    lastElement = newNode
		else:
		    lastElement.setNextNode(newNode)
		    lastElement = newNode
	elif isinstance(par1, PeacockPath):
#	    LOG.debug('initialize with PeacockPath')
	    lastElement = self.first
	    while lastElement.getNext() != None:
		lastElement = lastElement.getNext()
	    for i in range(0,par1.size()):
		elem = par1.getElement(i)
		lastElement.setNext(PeacockNode(elem))
		lastElement = lastElement.getNext()

    def mergeOdd(self, nodeFrom, nodeTo):
#	LOG.debug('PeacockPath merge nodes')
#	LOG.debug('nodeFrom: %s', nodeFrom.debugPrint())
#	LOG.debug('nodeTo: %s', nodeTo.debugPrint())
#	LOG.debug('path: %s', self.debugPrint())
	nodeTo.add(nodeFrom)
	if nodeFrom.equals(self.first):
	    self.first = nodeTo
	else:
	    node = self.first
	    nodeNext = self.first.getNextNode()
	    while not nodeNext.equals(nodeFrom):
		node = nodeNext
		nodeNext = node.getNextNode()
	    node.setNextNode(nodeTo)
#	LOG.debug('path after merge: %s', self.debugPrint())

    def mergeEven(self, nodeFrom, nodeTo):
	actNode = self.first
	while actNode != None:
	    if actNode.contains(nodeTo):
		actNode.add(nodeFrom)
		break
	    actNode = actNode.getNextNode()

    def getFirst(self):
#	LOG.debug('PeacockPath getFirst()')
	return self.first

    def getNext(self, node):
#	LOG.debug('PeacockPath getNext(%s)', node)
	actNode = self.first
	while actNode != None:
	    if actNode.equals(node):
		if actNode.getMainNum() == node.getMainNum():
		    return node.getNextNode()

	    actNode = actNode.getNextNode()

	return None

    def getPosition(self, elem):
#	LOG.debug('PeacockPath getPosition()')
	i = 0
	act = self.first
#	LOG.debug('are nodes %s, %s equal: %s', act.getElements(), elem.getElements(), act.equals(elem))
	while not act.equals(elem):
	    act = act.getNextNode()
	    if act == None:
		return -1
	    i = i + 1

	return i

    def getNode(self, par1):
#	LOG.debug('PeacockPath getNode(%s)', par1)
#	if isinstance(par1, int):
#	    LOG.debug('par1: int')
#	elif isinstance(par1, basestring):
#	    LOG.debug('par1: basestring')
#	elif isinstance(par1, PeacockNode):
#	    LOG.debug('par1: PeacockNode')

	if isinstance(par1, basestring):
	    par1 = int(par1)
	act = self.first
	if isinstance(par1, int):
	    while not act.contains(par1):
		act = act.getNextNode()
		if act == None:
		    return None

	    return act

	elif isinstance(par1, PeacockNode):
	    actNode = self.first
	    while not actNode.equals(par1):
		actNode = actNode.getNextNode()
		if actNode == None:
		    return None

	    return actNode

	else:
	    return None

    def getElement(self, i):
#	LOG.debug('PeacockPath getElement(%s)', i)
	act = self.first
	for j in range(0, i):
	    act = act.getNextNode()

	return act

    def getSize(self):
#	LOG.debug('PeacockPath getSize()')
	i = 0
	act = self.first
	while act != None:
	    act = act.getNextNode()
	    i = i + 1

	return i

    def contains(self, node):
#	LOG.debug('PeacockPath contains(%s)', node)
	actNode = self.first
	while actNode != None:
	    if actNode.contains(node):
		return True
	    actNode = actNode.getNextNode()

	return False

    def debugPrint(self):
#	LOG.debug('PeacockPath debugPrint()')
	ret = []
	actNode = self.first
	while actNode != None:
	    actNode.size()
	    ret.append(actNode.debugPrint())
	    actNode = actNode.getNextNode()

	return ret

    def getForwardEdges(self, path):
#	LOG.debug('PeacockPath getForwardEdges()')
#	LOG.debug('actPath: %s', self.debugPrint())
#	LOG.debug('referencePath: %s', path.debugPrint())
	forwardEdges = []
	actNode = self.first
	while actNode.getNextNode() != None:
#	    LOG.debug('actNode: %s', actNode.debugPrint())
	    nextNode = actNode.getNextNode()
#	    LOG.debug('nextNode: %s', actNode.getNextNode().debugPrint())
	    iActNode = path.getPosition(actNode)
	    iNextNode = path.getPosition(nextNode)
#	    LOG.debug('positions of actNode: %s and nextNode: %s', iActNode, iNextNode)
	    if iNextNode > iActNode:
		forwardEdges.append(actNode)
	    actNode = nextNode

	return forwardEdges

    def getDistanceOfNodeToNextNode(self, node, path):
#	LOG.debug('PeacockPath getDistanceOfNodeToNextNode(%s, %s)', node, path)
	iNode = path.getPosition(node)
	nextNode = node.getNextNode()
	iNextNode = path.getPosition(nextNode)
	return iNextNode - iNode

class PeacockNode(object):
#    elements = []
#    mainNum = 0
#    nextNode = None

    def __init__(self, elements):
#	LOG.debug('PeacockNode init(%s)', elements)
	self.elements = []
	self.mainNum = 0
	self.nextNode = None
	self.addElement(elements)

    def addElement(self, par1):
#	LOG.debug('add Elements to PeacockNode %s', par1)
	if isinstance(par1, list):
#	    LOG.debug('init PeacockNode from list')
	    for elem in par1:
		self.elements.append(int(elem))
	elif isinstance(par1, int):
#	    LOG.debug('init PeacockNode from int')
	    self.elements.append(par1)
	    if len(self.elements) == 1:
		self.mainNum = par1
	elif isinstance(par1, basestring):
#	    LOG.debug('init PeacockNode from basestring')
	    self.elements.append(int(par1))
	    if len(self.elements) == 1:
		self.mainNum = int(par1)
	elif isinstance(par1, PeacockNode):
#	    LOG.debug('init PeacockNode from PeacockNode')
	    for i in range(0,par1.size()):
		elem = par1.getElement(i)
		self.elements.append(int(elem))

	    self.mainNum = par1.getMainNum()

    def getNextNode(self):
#	LOG.debug('PeacockNode getNextNode()')
	return self.nextNode

    def setNextNode(self, node):
#	LOG.debug('PeacockNode setNextNode(%s)', node)
#	LOG.debug('actual node has %s elements', self.size())
#	LOG.debug('next node has %s elements', node.size())
	self.nextNode = node

    def add(self, par1):
#	LOG.debug('PeacockNode add(%s)', par1)
	for i in range(0,par1.size()):
	    self.elements.append(par1.getElement(i))

    def contains(self, par1):
#	LOG.debug('PeacockNode contains(%s) elements: %s', num, self.elements)
	if isinstance(par1, basestring):
	   par1 = int(par1)

	if isinstance(par1, int):
	    if par1 in self.elements:
		return True
	    else:
		return False
	elif isinstance(par1, PeacockNode):
	    for nodeNum in par1.elements:
		if not nodeNum in self.elements:
		    return False

	    return True

    def equals(self, node):
#	LOG.debug('PeacockNode equals(%s)', node)
	if node == None:
#	    LOG.debug('PeacockNode equals node = None')
	    return False

#	LOG.debug('%s == %s', self.elements, node.elements)
	if self.size() == node.size():
	    pass
	else:
	    return False

	for i in range(0,node.size()):
	    if self.elements[i] == node.getElement(i):
		pass
	    else:
		return False

	return True

    def getElement(self, i):
#	LOG.debug('PeacockNode getElement(%s)', i)
	return self.elements[i]

    def getElements(self):
	return list(self.elements)

    def size(self):
#	LOG.debug('PeacockNode size()')
#	LOG.debug('PeacockNode in this node are %s elements', len(self.elements))
	return len(self.elements)

    def getMainNum(self):
#	LOG.debug('PeacockNode getMainNum()')
	return self.mainNum

    def setMainNum(self, num):
#	LOG.debug('PeacockNode setMainNum(%s)', num)
	self.mainNum = num

    def debugPrint(self):
#	LOG.debug('PeacockNode debugPrint()')
	return self.elements

class Peacock(object):
#    first = []
#    oldpath = None
#    newpath = None
#    shedule = []
#    bothNum = []

    def __init__(self, oldpathNum, newpathNum):
	self.first = []
	self.shedule = []
	tmp = []
	for node in oldpathNum:
	    if node in newpathNum:
		tmp.append(node)
	    else:
		self.first.append(node)
	LOG.debug('initialize PeacockPath with %s', tmp)
	self.oldpath = PeacockPath(tmp)

	tmp = []
	for node in newpathNum:
	    if node in oldpathNum:
		tmp.append(node)
	    else:
		self.first.append(node)
	LOG.debug('initialize PeacockPath with %s', tmp)
	self.newpath = PeacockPath(tmp)

	self.bothNum = tmp

    def getNodesNotInShedule(self):
	tmp = list(self.bothNum)
	for r in self.shedule:
	    for node in r:
		if node in tmp:
		    tmp.remove(node)
	return tmp

    def sortForwardEdges(self, forwardEdges, newpath, oldpath):
	sortedEdges = []
	while len(forwardEdges) > 0:
	    longestNode = None
	    longestDistance = 0
	    for node in forwardEdges:
		actDistance = newpath.getDistanceOfNodeToNextNode(node, oldpath)
		if actDistance > longestDistance:
		    longestDistance = actDistance
		    longestNode = node
	    sortedEdges.append(longestNode)
	    forwardEdges.remove(longestNode)

	return sortedEdges

    def mergeNodesOdd(self, nodes):
	for nodeNum in nodes:
	    node = self.newpath.getNode(nodeNum)
	    nodeNext = self.newpath.getNext(node)
	    elementsNodeNext = nodeNext.getElements()
	    self.newpath.mergeOdd(node, nodeNext)
	    nodeNext = PeacockNode(elementsNodeNext)
	    node = self.oldpath.getNode(nodeNum)
	    nodeNext = self.oldpath.getNode(nodeNext)
	    self.oldpath.mergeOdd(node, nodeNext)

    def mergeNodesEven(self, nodes):
	tmpNodes = list(nodes)
	tmpNodes2 = list(nodes)
	while len(tmpNodes) > 0:
	    LOG.debug('mergeNodesEven tmpNodes: %s', tmpNodes)
	    for nodeNum in tmpNodes:
		node = self.newpath.getNode(nodeNum)
		nodeNext = self.newpath.getNext(node)
		LOG.debug('node: %s, nodeNext: %s', node.getElements(), nodeNext.getElements())
		LOG.debug('oldpath: %s', self.oldpath.debugPrint())
		LOG.debug('all nodes to merge: %s', tmpNodes2)
		if self.oldpath.contains(nodeNext):
		    self.oldpath.mergeEven(node, nodeNext)
		    tmpNodes.remove(nodeNum)

	for nodeNum in nodes:
	    node = self.newpath.getNode(nodeNum)
	    nodeNext = self.newpath.getNext(node)
	    self.newpath.mergeOdd(node, nodeNext)

    def peacock(self):
	LOG.debug('calculate peacock shedule')
	t = 0
	while True:
	    t = t + 1
	    LOG.debug('act peacock round: %s', t)
	    LOG.debug('act oldpath: %s', self.oldpath.debugPrint())
	    if self.oldpath.getSize() == 1:
		break

	    self.shedule.append([])

	    nodesNotInShedule = self.getNodesNotInShedule()
	    if t%2 == 1:
		forwardEdges = self.newpath.getForwardEdges(self.oldpath)
		LOG.debug('forwardEdges:')
		for node in forwardEdges:
		    LOG.debug('%s', node.debugPrint())
		sortedForwardEdges = self.sortForwardEdges(forwardEdges, self.newpath, self.oldpath)
		LOG.debug('sortedForwardEdges:')
		for node in sortedForwardEdges:
		    LOG.debug('%s', node.debugPrint())
		actRound = []
		for node in sortedForwardEdges:
		    LOG.debug('actNode in sortedForwaringNode: %s', node.debugPrint())
		    add = True
		    LOG.debug('length of actual shedule round: %s', len(actRound))
		    for nodeSheduleNum in actRound:
			LOG.debug('node in actRound: %s', nodeSheduleNum)
			nodeShedule = self.newpath.getNode(nodeSheduleNum)
			LOG.debug('nodeShedule: %s', nodeShedule)
#			LOG.debug('1')
			nodeNext = self.newpath.getNext(node)
#			LOG.debug('2')
			nodeSheduleNext = self.newpath.getNext(nodeShedule)
#			LOG.debug('3')
			iNode = self.oldpath.getPosition(node)
##			LOG.debug('4')
			iNodeNext = self.oldpath.getPosition(nodeNext)
#			LOG.debug('5')
			iNodeShedule = self.oldpath.getPosition(nodeShedule)
#			LOG.debug('6')
			iNodeSheduleNext = self.oldpath.getPosition(nodeSheduleNext)
			LOG.debug('%s < %s < %s', iNodeShedule, iNode, iNodeSheduleNext)
			if iNodeShedule < iNode:
			    if iNode < iNodeSheduleNext:
				LOG.debug('cant add %s', iNode)
				add = False
			LOG.debug('%s < %s < %s', iNodeShedule, iNodeNext, iNodeSheduleNext)
			if iNodeShedule < iNodeNext:
			    if iNodeNext < iNodeSheduleNext:
				LOG.debug('cant add %s', iNode)
				add = False
		    if add:
			LOG.debug('add %s to actRound', node.getMainNum())
			actRound.append(node.getMainNum())

		LOG.debug('calculation of round finished: %s', actRound)
		for num in actRound:
		    self.shedule[t-1].append(num)

		if t == 1:
		    for nodeNum in self.first:
			self.shedule[t-1].append(nodeNum)
		LOG.debug('merge nodes %s', actRound)
		self.mergeNodesOdd(actRound)

		LOG.debug('oldpath: %s', self.oldpath.debugPrint())
		LOG.debug('newpath: %s', self.newpath.debugPrint())

	    else:
		LOG.debug('oldpath: %s', self.oldpath.debugPrint())
		LOG.debug('newpath: %s', self.newpath.debugPrint())
		actRound = []
		for i in range(0,self.newpath.getSize()):
		    newNode = self.newpath.getElement(i)
		    if self.oldpath.contains(newNode):
			pass
		    else:
			actRound.append(newNode.getMainNum())

		for numNode in actRound:
		    self.shedule[t-1].append(numNode)
		
		self.mergeNodesEven(actRound)

	i = 1
	for r in self.shedule:
	    LOG.debug('round %s is: %s', i, r)

	return self.shedule

class StatsController(ControllerBase):
#class StatsController(app_manager.RyuApp):
    def __init__(self, req, link, data, **config):
        super(StatsController, self).__init__(req, link, data, **config)
        self.dpset = data['dpset']
        self.waiters = data['waiters']

    def get_dpids(self, req, **_kwargs):
        dps = self.dpset.dps.keys()
        body = json.dumps(dps)
        return Response(content_type='application/json', body=body)

    def get_desc_stats(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)
        _ofp_version = dp.ofproto.OFP_VERSION

        _ofctl = supported_ofctl.get(_ofp_version, None)
        if _ofctl is not None:
            desc = _ofctl.get_desc_stats(dp, self.waiters)

        else:
            LOG.debug('Unsupported OF protocol')
            return Response(status=501)

        body = json.dumps(desc)
        return Response(content_type='application/json', body=body)

    def get_flow_stats(self, req, dpid, **_kwargs):

        if req.body == '':
            flow = {}

        else:

            try:
                flow = ast.literal_eval(req.body)

            except SyntaxError:
                LOG.debug('invalid syntax %s', req.body)
                return Response(status=400)

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION

        _ofctl = supported_ofctl.get(_ofp_version, None)
        if _ofctl is not None:
            flows = _ofctl.get_flow_stats(dp, self.waiters, flow)

        else:
            LOG.debug('Unsupported OF protocol')
            return Response(status=501)

        body = json.dumps(flows)
        return Response(content_type='application/json', body=body)

    def get_aggregate_flow_stats(self, req, dpid, **_kwargs):

        if req.body == '':
            flow = {}

        else:
            try:
                flow = ast.literal_eval(req.body)

            except SyntaxError:
                LOG.debug('invalid syntax %s', req.body)
                return Response(status=400)

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION

        _ofctl = supported_ofctl.get(_ofp_version, None)
        if _ofctl is not None:
            flows = _ofctl.get_aggregate_flow_stats(dp, self.waiters, flow)

        else:
            LOG.debug('Unsupported OF protocol')
            return Response(status=501)

        body = json.dumps(flows)
        return Response(content_type='application/json', body=body)

    def get_port_stats(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION

        _ofctl = supported_ofctl.get(_ofp_version, None)
        if _ofctl is not None:
            ports = _ofctl.get_port_stats(dp, self.waiters)

        else:
            LOG.debug('Unsupported OF protocol')
            return Response(status=501)

        body = json.dumps(ports)
        return Response(content_type='application/json', body=body)

    def get_queue_stats(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION

        _ofctl = supported_ofctl.get(_ofp_version, None)
        if _ofctl is not None:
            queues = _ofctl.get_queue_stats(dp, self.waiters)

        else:
            LOG.debug('Unsupported OF protocol')
            return Response(status=501)

        body = json.dumps(queues)
        return Response(content_type='application/json', body=body)

    def get_meter_features(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION
        _ofctl = supported_ofctl.get(_ofp_version, None)

        if _ofctl is not None and hasattr(_ofctl, 'get_meter_features'):
            meters = _ofctl.get_meter_features(dp, self.waiters)

        else:
            LOG.debug('Unsupported OF protocol or \
                request not supported in this OF protocol version')
            return Response(status=501)

        body = json.dumps(meters)
        return Response(content_type='application/json', body=body)

    def get_meter_config(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION
        _ofctl = supported_ofctl.get(_ofp_version, None)

        if _ofctl is not None and hasattr(_ofctl, 'get_meter_config'):
            meters = _ofctl.get_meter_config(dp, self.waiters)

        else:
            LOG.debug('Unsupported OF protocol or \
                request not supported in this OF protocol version')
            return Response(status=501)

        body = json.dumps(meters)
        return Response(content_type='application/json', body=body)

    def get_meter_stats(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION
        _ofctl = supported_ofctl.get(_ofp_version, None)

        if _ofctl is not None and hasattr(_ofctl, 'get_meter_stats'):
            meters = _ofctl.get_meter_stats(dp, self.waiters)

        else:
            LOG.debug('Unsupported OF protocol or \
                request not supported in this OF protocol version')
            return Response(status=501)

        body = json.dumps(meters)
        return Response(content_type='application/json', body=body)

    def get_group_features(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION
        _ofctl = supported_ofctl.get(_ofp_version, None)

        if _ofctl is not None and hasattr(_ofctl, 'get_group_features'):
            groups = _ofctl.get_group_features(dp, self.waiters)

        else:
            LOG.debug('Unsupported OF protocol or \
                request not supported in this OF protocol version')
            return Response(status=501)

        body = json.dumps(groups)
        return Response(content_type='application/json', body=body)

    def get_group_desc(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION
        _ofctl = supported_ofctl.get(_ofp_version, None)

        if _ofctl is not None and hasattr(_ofctl, 'get_group_desc'):
            groups = _ofctl.get_group_desc(dp, self.waiters)

        else:
            LOG.debug('Unsupported OF protocol or \
                request not supported in this OF protocol version')
            return Response(status=501)

        body = json.dumps(groups)
        return Response(content_type='application/json', body=body)

    def get_group_stats(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION
        _ofctl = supported_ofctl.get(_ofp_version, None)

        if _ofctl is not None and hasattr(_ofctl, 'get_group_stats'):
            groups = _ofctl.get_group_stats(dp, self.waiters)

        else:
            LOG.debug('Unsupported OF protocol or \
                request not supported in this OF protocol version')
            return Response(status=501)

        body = json.dumps(groups)
        return Response(content_type='application/json', body=body)

    def get_port_desc(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION

        _ofctl = supported_ofctl.get(_ofp_version, None)
        if _ofctl is not None:
            groups = _ofctl.get_port_desc(dp, self.waiters)

        else:
            LOG.debug('Unsupported OF protocol')
            return Response(status=501)

        body = json.dumps(groups)
        return Response(content_type='application/json', body=body)

    def mod_flow_entry(self, req, cmd, **_kwargs):
        try:
            flow = ast.literal_eval(req.body)

        except SyntaxError:
            LOG.debug('invalid syntax %s', req.body)
            return Response(status=400)

        dpid = flow.get('dpid')

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        if cmd == 'add':
            cmd = dp.ofproto.OFPFC_ADD
        elif cmd == 'modify':
            cmd = dp.ofproto.OFPFC_MODIFY
        elif cmd == 'modify_strict':
            cmd = dp.ofproto.OFPFC_MODIFY_STRICT
        elif cmd == 'delete':
            cmd = dp.ofproto.OFPFC_DELETE
        elif cmd == 'delete_strict':
            cmd = dp.ofproto.OFPFC_DELETE_STRICT
        else:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION
        _ofctl = supported_ofctl.get(_ofp_version, None)
        if _ofctl is not None:
            _ofctl.mod_flow_entry(dp, flow, cmd)
#	    pass
        else:
            LOG.debug('Unsupported OF protocol')
            return Response(status=501)

        return Response(status=200)

# start: own shedule random delay
    def update_flow_entry_own_shedule_random_delay(self, req, **_kwargs):
        LOG.debug('external call: update flow entry own shedule random delay')
        lines = req.body.split('\n')
        p = re.compile('[0-9,]')

        first = []
        second = []
        third = []
        fourth = []
        fifth = []
	roundTime = 0

        for i in range(0,7):
            if "first" in lines[i]:
                first_str = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')
                for elem in first_str:
                    if elem != '':
                        first.append(int(elem))

            if "second" in lines[i]:
                second_str = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')
                for elem in second_str:
                    if elem != '':
                        second.append(int(elem))

            if "third" in lines[i]:
                third_str = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')
                for elem in third_str:
                    if elem != '':
                        third.append(int(elem))

            if "fourth" in lines[i]:
                fourth_str = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')
                for elem in fourth_str:
                    if elem != '':
                        fourth.append(int(elem))

            if "fifth" in lines[i]:
                fifth_str = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')
                for elem in fifth_str:
                    if elem != '':
                        fifth.append(int(elem))

            if "roundTime" in lines[i]:
		roundTime = str(lines[i].split(':')[1][:-1])

#       LOG.debug(first)
#       LOG.debug(second)
#       LOG.debug(third)
#       LOG.debug(fourth)
#       LOG.debug(fifth)

	lines_cleared = lines[0]

        for i in range(6,len(lines)):
            lines_cleared = lines_cleared + lines[i]

#       LOG.debug(lines_cleared)

        msg = lines_cleared

        occurence = req.body.count('"dpid":')
        flowentries = []

        for i in range(0,occurence):
            index_dpid = msg.find('"dpid":')
            index_comma = msg.find(',', index_dpid, len(msg))
            numbers = re.compile('[0-9]')
            dpid_num = "".join(numbers.findall(msg[index_dpid:index_comma]))

            index_bracket_open = msg[0:index_dpid].rfind('{')

            index_quote_2 = msg[0:index_bracket_open].rfind('"')
            index_quote_1 = msg[0:index_quote_2].rfind('"')+1

            brackets = 1
            index_bracket_close = 0

            for j in range(index_comma,len(msg)):
                if(msg[j] == '{'):
                    brackets = brackets + 1

                if(msg[j] == '}'):
                    brackets = brackets - 1

                if brackets == 0:
                    index_bracket_close = j
                    break

            flowentry = {}
            flowentry[msg[index_quote_1:index_quote_2] + "," + dpid_num] = msg[index_bracket_open:index_bracket_close+1]
            flowentries.append(flowentry)
            msg = msg[0:index_quote_1] + msg[index_bracket_close:len(msg)]

        item = OwnSheduleRandomDelayMessage(first, second, third, fourth, fifth, flowentries, self.dpset, self, roundTime)
        queue.push(item)

#       LOG.debug(msg)
#       LOG.debug(flowentries)

        message = queue.first()
        if not message.getInProgress():
            message.setInProgress()
            message.send_next_round()


# start: own shedule
    def update_flow_entry_own_shedule(self, req, **_kwargs):
	LOG.debug('external call: update flow entry own shedule')
	lines = req.body.split('\n')
	p = re.compile('[0-9,]')

	first = []
	second = []
	third = []
	fourth = []
	fifth = []

	for i in range(0,6):
	    if "first" in lines[i]:
		first_str = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')
		for elem in first_str:
		    if elem != '':
			first.append(int(elem))

	    if "second" in lines[i]:
		second_str = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')
                for elem in second_str:
		    if elem != '':
			second.append(int(elem))

	    if "third" in lines[i]:
		third_str = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')
                for elem in third_str:
		    if elem != '':
			third.append(int(elem))

	    if "fourth" in lines[i]:
		fourth_str = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')
                for elem in fourth_str:
		    if elem != '':
                	fourth.append(int(elem))

	    if "fifth" in lines[i]:
		fifth_str = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')
                for elem in fifth_str:
		    if elem != '':
			fifth.append(int(elem))

#	LOG.debug(first)
#	LOG.debug(second)
#	LOG.debug(third)
#	LOG.debug(fourth)
#	LOG.debug(fifth)

	lines_cleared = lines[0]

        for i in range(6,len(lines)):
            lines_cleared = lines_cleared + lines[i]

#       LOG.debug(lines_cleared)

	msg = lines_cleared

        occurence = req.body.count('"dpid":')
        flowentries = []

        for i in range(0,occurence):
            index_dpid = msg.find('"dpid":')
            index_comma = msg.find(',', index_dpid, len(msg))
            numbers = re.compile('[0-9]')
            dpid_num = "".join(numbers.findall(msg[index_dpid:index_comma]))

            index_bracket_open = msg[0:index_dpid].rfind('{')

            index_quote_2 = msg[0:index_bracket_open].rfind('"')
            index_quote_1 = msg[0:index_quote_2].rfind('"')+1

            brackets = 1
            index_bracket_close = 0

            for j in range(index_comma,len(msg)):
                if(msg[j] == '{'):
                    brackets = brackets + 1

                if(msg[j] == '}'):
                    brackets = brackets - 1

                if brackets == 0:
                    index_bracket_close = j
                    break

            flowentry = {}
            flowentry[msg[index_quote_1:index_quote_2] + "," + dpid_num] = msg[index_bracket_open:index_bracket_close+1]
            flowentries.append(flowentry)
            msg = msg[0:index_quote_1] + msg[index_bracket_close:len(msg)]

        item = OwnSheduleMessage(first, second, third, fourth, fifth, flowentries, self.dpset, self)
        queue.push(item)

#       LOG.debug(msg)
#       LOG.debug(flowentries)

        message = queue.first()
        if not message.getInProgress():
            message.setInProgress()
            message.send_next_round()

# start: WayUp
# (barrier request)
# http://ryu.readthedocs.org/en/latest/ofproto_v1_2_ref.html

    def update_flow_entry(self, req, **_kwargs):
	LOG.debug('external call: update flow entry')


	now = datetime.now()
        timefile.write("rest WayUp message recieved: " + str(now) + "\n")
	timefile.flush()



	lines = req.body.split('\n')
	p = re.compile('[0-9,]')

	oldpath = []
	newpath = []
	wp = 0
	interval = 0

	for i in range(0,4):
	    if "oldpath" in lines[i]:
		oldpath = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')

	    if "newpath" in lines[i]:
		newpath = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')

	    if "wp" in lines[i]:
		wp = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')[0]

	    if "interval" in lines[i]:
		interval = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')[0]

	if not wp in oldpath:
	    return

	if not wp in newpath:
	    return

#	FIRST
	first = []
	for node in newpath:
	    if not node in oldpath:
		first.append(node)

#	SECOND
	oldpathBehindWP = []
	oldpathInfrontofWP = []
	passed = False
	for node in oldpath:
	    if node == wp:
		passed = True
		continue
	    if passed:
		oldpathBehindWP.append(node)
	    else:
		oldpathInfrontofWP.append(node)

	newpathBehindWP = []
	newpathInfrontofWP = []
	passed = False
	for node in newpath:
	    if node == wp:
		passed = True
		continue
	    if passed:
		newpathBehindWP.append(node)
	    else:
		newpathInfrontofWP.append(node)

	nodesWithBackwardRule = []
	for fromNode in newpath:
	    index = newpath.index(fromNode)
	    if len(newpath) == index + 1:
		break
	    toNode = newpath[index+1]
	    if fromNode in oldpath and toNode in oldpath:
		if oldpath.index(fromNode)>oldpath.index(toNode):
		    nodesWithBackwardRule.append(fromNode)

	second = []
	for node in oldpathBehindWP:
	    if node in newpathInfrontofWP:
		if node in nodesWithBackwardRule:
		    if not node in first:
			second.append(node)

#	THIRD
	third = []
	for node in newpathInfrontofWP:
	    if not node in second:
		if not node in first:
		    third.append(node)
	third.append(wp)

#	fourth
	fourth = []
	for node in newpathBehindWP:
	    if not node in first:
		fourth.append(node)


#	LOG.debug('first: %s', first)
#	LOG.debug('second: %s', second)
#	LOG.debug('third: %s', third)
#	LOG.debug('fourth: %s', fourth)

	lines_cleared = lines[0]

	for i in range(5,len(lines)):
	    lines_cleared = lines_cleared + lines[i]

#	msg_no_newline = re.compile('[A-Za-z0-9,\[\]{}:"]')
#	LOG.debug('%s', "".join(msg_no_newline.findall(lines_cleared)))
#	msg = "".join(msg_no_newline.findall(lines_cleared))
	msg = lines_cleared

	occurence = req.body.count('"dpid":')
	flowentries = []

	for i in range(0,occurence):
	    index_dpid = msg.find('"dpid":')
	    index_comma = msg.find(',', index_dpid, len(msg))
	    numbers = re.compile('[0-9]')
	    dpid_num = "".join(numbers.findall(msg[index_dpid:index_comma]))
	    
	    index_bracket_open = msg[0:index_dpid].rfind('{')

	    index_quote_2 = msg[0:index_bracket_open].rfind('"')
	    index_quote_1 = msg[0:index_quote_2].rfind('"')+1

	    brackets = 1
	    index_bracket_close = 0

	    for j in range(index_comma,len(msg)):
		if(msg[j] == '{'):
		    brackets = brackets + 1
		
		if(msg[j] == '}'):
		    brackets = brackets - 1

		if brackets == 0:
		    index_bracket_close = j
		    break

	    flowentry = {}
	    flowentry[msg[index_quote_1:index_quote_2] + "," + dpid_num] = msg[index_bracket_open:index_bracket_close+1]
	    flowentries.append(flowentry)
	    msg = msg[0:index_quote_1] + msg[index_bracket_close:len(msg)]

	item = WayUpMessage(first, second, third, fourth, interval, flowentries, self.dpset, self)
	queue.push(item)

#	LOG.debug(msg)
#	LOG.debug(flowentries)

	message = queue.first()
	if not message.getInProgress():
	    message.setInProgress()
	    message.send_next_round()

    def send_flow_mod_message(self, cmd, message):
	LOG.debug('message type %s', cmd)
	LOG.debug('send flow mod message')
	try:
            flow = ast.literal_eval(message)
        except SyntaxError:
            LOG.debug('return: invalid syntax %s', message)
            return Response(status=400)

        dpid = flow.get('dpid')
	LOG.debug('send message to %s',dpid)

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('return: invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
	    LOG.debug('return: dpid not found')
            return Response(status=404)

        if cmd == 'add':
            cmd = dp.ofproto.OFPFC_ADD
        elif cmd == 'modify':
            cmd = dp.ofproto.OFPFC_MODIFY
        elif cmd == 'modify_strict':
            cmd = dp.ofproto.OFPFC_MODIFY_STRICT
        elif cmd == 'delete':
            cmd = dp.ofproto.OFPFC_DELETE
        elif cmd == 'delete_strict':
            cmd = dp.ofproto.OFPFC_DELETE_STRICT
        else:
	    LOG.debug('return: message type not found')
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION
        _ofctl = supported_ofctl.get(_ofp_version, None)
        if _ofctl is not None:
	    LOG.debug('send: Send flow mod message to switch')
            _ofctl.mod_flow_entry(dp, flow, cmd)
        else:
            LOG.debug('return: Unsupported OF protocol')
            return Response(status=501)

    def update_flow_entry_peacock(self, req, **_kwargs):
	LOG.debug('external call: update flow entry peacock')
        lines = req.body.split('\n')
        p = re.compile('[0-9,]')

        oldpath = []
        newpath = []

        for i in range(0,3):
            if "oldpath" in lines[i]:
                oldpath = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')

            if "newpath" in lines[i]:
                newpath = "".join(p.findall(lines[i].split(':')[1][:-1])).split(',')

	processor = Peacock(oldpath, newpath)
	shedule = processor.peacock()

	i = 0
	for r in shedule:
	    LOG.debug(i)
	    LOG.debug(r)
	    i = i + 1

	lines_cleared = lines[0]

        for i in range(3,len(lines)):
            lines_cleared = lines_cleared + lines[i]

#       msg_no_newline = re.compile('[A-Za-z0-9,\[\]{}:"]')
#       LOG.debug('%s', "".join(msg_no_newline.findall(lines_cleared)))
#       msg = "".join(msg_no_newline.findall(lines_cleared))
        msg = lines_cleared

        occurence = req.body.count('"dpid":')
        flowentries = []

        for i in range(0,occurence):
            index_dpid = msg.find('"dpid":')
            index_comma = msg.find(',', index_dpid, len(msg))
            numbers = re.compile('[0-9]')
            dpid_num = "".join(numbers.findall(msg[index_dpid:index_comma]))

            index_bracket_open = msg[0:index_dpid].rfind('{')

            index_quote_2 = msg[0:index_bracket_open].rfind('"')
            index_quote_1 = msg[0:index_quote_2].rfind('"')+1

            brackets = 1
            index_bracket_close = 0

            for j in range(index_comma,len(msg)):
                if(msg[j] == '{'):
                    brackets = brackets + 1

                if(msg[j] == '}'):
                    brackets = brackets - 1

                if brackets == 0:
                    index_bracket_close = j
                    break

            flowentry = {}
            flowentry[msg[index_quote_1:index_quote_2] + "," + dpid_num] = msg[index_bracket_open:index_bracket_close+1]
            flowentries.append(flowentry)
            msg = msg[0:index_quote_1] + msg[index_bracket_close:len(msg)]

        item = PeacockMessage(shedule, flowentries, self.dpset, self)
        queue.push(item)

	message = queue.first()
        if not message.getInProgress():
            message.setInProgress()
            message.sendNextRoundFlowEntries()

    def delete_flow_entry(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        flow = {'table_id': dp.ofproto.OFPTT_ALL}

        _ofp_version = dp.ofproto.OFP_VERSION

        _ofctl = supported_ofctl.get(_ofp_version, None)
        if _ofctl is not None:
            _ofctl.mod_flow_entry(dp, flow, dp.ofproto.OFPFC_DELETE)

        else:
            LOG.debug('Unsupported OF protocol')
            return Response(status=501)

        return Response(status=200)

    def mod_meter_entry(self, req, cmd, **_kwargs):

        try:
            flow = ast.literal_eval(req.body)

        except SyntaxError:
            LOG.debug('invalid syntax %s', req.body)
            return Response(status=400)

        dpid = flow.get('dpid')

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        if cmd == 'add':
            cmd = dp.ofproto.OFPMC_ADD
        elif cmd == 'modify':
            cmd = dp.ofproto.OFPMC_MODIFY
        elif cmd == 'delete':
            cmd = dp.ofproto.OFPMC_DELETE
        else:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION
        _ofctl = supported_ofctl.get(_ofp_version, None)

        if _ofctl is not None and hasattr(_ofctl, 'mod_meter_entry'):
            _ofctl.mod_meter_entry(dp, flow, cmd)

        else:
            LOG.debug('Unsupported OF protocol or \
                request not supported in this OF protocol version')
            return Response(status=501)

        return Response(status=200)

    def mod_group_entry(self, req, cmd, **_kwargs):

        try:
            group = ast.literal_eval(req.body)

        except SyntaxError:
            LOG.debug('invalid syntax %s', req.body)
            return Response(status=400)

        dpid = group.get('dpid')

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        if cmd == 'add':
            cmd = dp.ofproto.OFPGC_ADD
        elif cmd == 'modify':
            cmd = dp.ofproto.OFPGC_MODIFY
        elif cmd == 'delete':
            cmd = dp.ofproto.OFPGC_DELETE
        else:
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION
        _ofctl = supported_ofctl.get(_ofp_version, None)

        if _ofctl is not None and hasattr(_ofctl, 'mod_group_entry'):
            _ofctl.mod_group_entry(dp, group, cmd)

        else:
            LOG.debug('Unsupported OF protocol or \
                request not supported in this OF protocol version')
            return Response(status=501)

        return Response(status=200)

    def mod_port_behavior(self, req, cmd, **_kwargs):

        try:
            port_config = ast.literal_eval(req.body)

        except SyntaxError:
            LOG.debug('invalid syntax %s', req.body)
            return Response(status=400)

        dpid = port_config.get('dpid')

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        port_no = port_config.get('port_no', 0)
        if type(port_no) == str and not port_no.isdigit():
            LOG.debug('invalid port_no %s', port_no)
            return Response(status=400)

        port_info = self.dpset.port_state[int(dpid)].get(port_no)

        if port_info:
            port_config.setdefault('hw_addr', port_info.hw_addr)
            port_config.setdefault('advertise', port_info.advertised)
        else:
            return Response(status=404)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        if cmd != 'modify':
            return Response(status=404)

        _ofp_version = dp.ofproto.OFP_VERSION

        _ofctl = supported_ofctl.get(_ofp_version, None)
        if _ofctl is not None:
            _ofctl.mod_port_behavior(dp, port_config)

        else:
            LOG.debug('Unsupported OF protocol')
            return Response(status=501)

        return Response(status=200)

    def send_experimenter(self, req, dpid, **_kwargs):

        if type(dpid) == str and not dpid.isdigit():
            LOG.debug('invalid dpid %s', dpid)
            return Response(status=400)

        dp = self.dpset.get(int(dpid))

        if dp is None:
            return Response(status=404)

        try:
            exp = ast.literal_eval(req.body)

        except SyntaxError:
            LOG.debug('invalid syntax %s', req.body)
            return Response(status=400)

        _ofp_version = dp.ofproto.OFP_VERSION
        _ofctl = supported_ofctl.get(_ofp_version, None)

        if _ofctl is not None and hasattr(_ofctl, 'send_experimenter'):
            _ofctl.send_experimenter(dp, exp)

        else:
            LOG.debug('Unsupported OF protocol')
            return Response(status=501)

        return Response(status=200)


class RestStatsApi(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_0.OFP_VERSION,
                    ofproto_v1_2.OFP_VERSION,
                    ofproto_v1_3.OFP_VERSION]
    _CONTEXTS = {
        'dpset': dpset.DPSet,
        'wsgi': WSGIApplication
    }

    def __init__(self, *args, **kwargs):
        super(RestStatsApi, self).__init__(*args, **kwargs)
        self.dpset = kwargs['dpset']
        wsgi = kwargs['wsgi']
        self.waiters = {}
        self.data = {}
        self.data['dpset'] = self.dpset
        self.data['waiters'] = self.waiters
        mapper = wsgi.mapper

        wsgi.registory['StatsController'] = self.data
        path = '/stats'
        uri = path + '/switches'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_dpids',
                       conditions=dict(method=['GET']))

        uri = path + '/desc/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_desc_stats',
                       conditions=dict(method=['GET']))

        uri = path + '/flow/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_flow_stats',
                       conditions=dict(method=['GET', 'POST']))

        uri = path + '/aggregateflow/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController,
                       action='get_aggregate_flow_stats',
                       conditions=dict(method=['GET', 'POST']))

        uri = path + '/port/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_port_stats',
                       conditions=dict(method=['GET']))

        uri = path + '/queue/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_queue_stats',
                       conditions=dict(method=['GET']))

        uri = path + '/meterfeatures/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_meter_features',
                       conditions=dict(method=['GET']))

        uri = path + '/meterconfig/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_meter_config',
                       conditions=dict(method=['GET']))

        uri = path + '/meter/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_meter_stats',
                       conditions=dict(method=['GET']))

        uri = path + '/groupfeatures/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_group_features',
                       conditions=dict(method=['GET']))

        uri = path + '/groupdesc/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_group_desc',
                       conditions=dict(method=['GET']))

        uri = path + '/group/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_group_stats',
                       conditions=dict(method=['GET']))

        uri = path + '/portdesc/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='get_port_desc',
                       conditions=dict(method=['GET']))

        uri = path + '/flowentry/{cmd}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='mod_flow_entry',
                       conditions=dict(method=['POST']))

        uri = path + '/flowupdate'
        mapper.connect('stats', uri,
                       controller=StatsController, action='update_flow_entry',
                       conditions=dict(method=['POST']))

	uri = path + '/flowupdate_peacock'
	mapper.connect('stats', uri,
		       controller=StatsController, action='update_flow_entry_peacock',
		       conditions=dict(method=['POST']))

	uri = path + '/flowupdate_own_shedule'
	mapper.connect('stats', uri,
		       controller=StatsController, action='update_flow_entry_own_shedule',
		       conditions=dict(method=['POST']))

	uri = path + '/flowupdate_own_shedule_random_delay'
        mapper.connect('stats', uri,
                       controller=StatsController, action='update_flow_entry_own_shedule_random_delay',
                       conditions=dict(method=['POST']))

        uri = path + '/flowentry/clear/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='delete_flow_entry',
                       conditions=dict(method=['DELETE']))

        uri = path + '/meterentry/{cmd}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='mod_meter_entry',
                       conditions=dict(method=['POST']))

        uri = path + '/groupentry/{cmd}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='mod_group_entry',
                       conditions=dict(method=['POST']))

        uri = path + '/portdesc/{cmd}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='mod_port_behavior',
                       conditions=dict(method=['POST']))

        uri = path + '/experimenter/{dpid}'
        mapper.connect('stats', uri,
                       controller=StatsController, action='send_experimenter',
                       conditions=dict(method=['POST']))

    @set_ev_cls([ofp_event.EventOFPStatsReply,
                 ofp_event.EventOFPDescStatsReply,
                 ofp_event.EventOFPFlowStatsReply,
                 ofp_event.EventOFPAggregateStatsReply,
                 ofp_event.EventOFPPortStatsReply,
                 ofp_event.EventOFPQueueStatsReply,
                 ofp_event.EventOFPMeterStatsReply,
                 ofp_event.EventOFPMeterFeaturesStatsReply,
                 ofp_event.EventOFPMeterConfigStatsReply,
                 ofp_event.EventOFPGroupStatsReply,
                 ofp_event.EventOFPGroupFeaturesStatsReply,
                 ofp_event.EventOFPGroupDescStatsReply,
                 ofp_event.EventOFPPortDescStatsReply
                 ], MAIN_DISPATCHER)
    def stats_reply_handler(self, ev):
        msg = ev.msg
        dp = msg.datapath

        if dp.id not in self.waiters:
            return
        if msg.xid not in self.waiters[dp.id]:
            return
        lock, msgs = self.waiters[dp.id][msg.xid]
        msgs.append(msg)

        flags = 0
        if dp.ofproto.OFP_VERSION == ofproto_v1_0.OFP_VERSION:
            flags = dp.ofproto.OFPSF_REPLY_MORE
        elif dp.ofproto.OFP_VERSION == ofproto_v1_2.OFP_VERSION:
            flags = dp.ofproto.OFPSF_REPLY_MORE
        elif dp.ofproto.OFP_VERSION == ofproto_v1_3.OFP_VERSION:
            flags = dp.ofproto.OFPMPF_REPLY_MORE

        if msg.flags & flags:
            return
        del self.waiters[dp.id][msg.xid]
        lock.set()

    @set_ev_cls(ofp_event.EventOFPBarrierReply, MAIN_DISPATCHER)
    def barrier_reply_handler(self, ev):
        dpid = ev.msg.datapath
        LOG.debug('recieve barrier reply from %s', dpid.id)
        message = queue.first()
        if isinstance(message, WayUpMessage):
	    step = message.getRound()
            ret = message.addReply(dpid.id)
	    if not ret:
		LOG.debug('something went wrong while recieving the barrier reply')
	    else:
		LOG.debug('barrier reply successfully received')

            newStep = message.getRound()
	    LOG.debug('step: %s newStep: %s', step, newStep)
            if step != newStep:

        	if newStep == 5:


                    now = datetime.now()
                    timefile.write("rest WayUp message finished: " + str(now) + "\n\n")
                    timefile.flush()


                    queue.delete()
                    LOG.debug('Message processed')
                    message = queue.first()
		    if message != None:
			message.setInProgress()
			message.send_next_round()

		else:
		    message.send_next_round()

        if isinstance(message, OwnSheduleMessage):
            step = message.getRound()
            ret = message.addReply(dpid.id)
            if not ret:
                LOG.debug('something went wrong while recieving the barrier reply')
            else:
                LOG.debug('barrier reply successfully received')

            newStep = message.getRound()
            LOG.debug('step: %s newStep: %s', step, newStep)
            if step != newStep:
                if newStep == 6:
                    queue.delete()
                    LOG.debug('Message processed')
                    message = queue.first()
                    if message != None:
                        message.setInProgress()
                        message.send_next_round()

                else:
                    message.send_next_round()

        if isinstance(message, OwnSheduleRandomDelayMessage):
            step = message.getRound()
            ret = message.addReply(dpid.id)
            if not ret:
                LOG.debug('something went wrong while recieving the barrier reply')
            else:
                LOG.debug('barrier reply successfully received')

            newStep = message.getRound()
            LOG.debug('step: %s newStep: %s', step, newStep)
            if step != newStep:
                if newStep == 6:
                    queue.delete()
                    LOG.debug('Message processed')
                    message = queue.first()
                    if message != None:
                        message.setInProgress()
                        message.send_next_round()

                else:
                    message.send_next_round()

	elif isinstance(message, PeacockMessage):
	    oldRound = message.getRound()
	    message.addReply(dpid.id)
	    newRound = message.getRound()
	    if not message.getInProgress():
		queue.delete()
		message = queue.first()
		if message != None:
		    message.setInProgress()
		    message.sendNextRoundFlowEntries()

    @set_ev_cls([ofp_event.EventOFPSwitchFeatures], MAIN_DISPATCHER)
    def features_reply_handler(self, ev):
        msg = ev.msg
        dp = msg.datapath

        if dp.id not in self.waiters:
            return
        if msg.xid not in self.waiters[dp.id]:
            return
        lock, msgs = self.waiters[dp.id][msg.xid]
        msgs.append(msg)

        del self.waiters[dp.id][msg.xid]
        lock.set()
