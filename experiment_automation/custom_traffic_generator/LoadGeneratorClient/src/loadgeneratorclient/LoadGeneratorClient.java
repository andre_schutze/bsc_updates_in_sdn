/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loadgeneratorclient;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Random;

/**
 *
 * @author andre
 */
public class LoadGeneratorClient {

    
    private static String getBytesToSend() {
//        StringBuilder ret = new StringBuilder();
//        String ret = new String();
        char[] ret = new char[60001000];
        int pointer = 0;
        Random randomGenerator = new Random();
        while (pointer < 60000000){
            int randomInt = randomGenerator.nextInt(Integer.MAX_VALUE);
//            System.out.println("randomInt: " + randomInt);
            String tmpS = String.valueOf(randomInt);
//            System.out.println("string of randomInt: " + tmpS);
            tmpS.getChars(0, tmpS.length(), ret, pointer);
//            for(int i = 0; i<newChars.length; i++) {
//                ret[pointer + i] = newChars[i];
//            }

//            String appended = new String();
//            for(int i = pointer; i<pointer+tmpS.length(); i++) {
//                appended = appended + ret[i];
//            }
//            System.out.println("appended: " + appended);

            pointer = pointer + tmpS.length();
//            System.out.println("pointer: " + pointer);
//            ret.append(String.valueOf(randomInt));
//            ret = ret + String.valueOf(randomInt);
        }
        return new String(ret);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("usage: LoadGeneratorClient.jar <server-ip-address>");
            return;
        }
        
        String sentence = getBytesToSend();
        System.out.println("start sending " + sentence.length() + " characters");
        Socket clientSocket = new Socket(args[0], 6789);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        outToServer.writeBytes(sentence + '\n');
        clientSocket.close();
    }
}
