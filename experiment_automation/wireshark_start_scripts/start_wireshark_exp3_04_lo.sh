echo "RUN SCRIPT: start_wireshark_exp3_04.sh"


experiment_number=$1
networkupdate_number=$2

# tshark: The file "/home/mininet/Documents/mininet_experiment5_01_x0.005_001_own_shedule_random_delay.pcapng" doesn't exist.

sudo touch /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`_own.pcapng
sudo chown mininet:mininet /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`_own.pcapng
sudo chmod 777 /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`_own.pcapng
sudo tshark -i s1-eth1 -i lo -c 2000 -w /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`_own.pcapng
