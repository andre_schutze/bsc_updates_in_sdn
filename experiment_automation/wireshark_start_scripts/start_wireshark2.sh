echo "RUN SCRIPT: start_wireshark2.sh"


experiment_number=$1
round_time=$2
networkupdate_number=$3

# tshark: The file "/home/mininet/Documents/mininet_experiment5_01_x0.005_001_own_shedule_random_delay.pcapng" doesn't exist.

sudo touch /home/mininet/Documents/mininet_experiment5_`echo $networkupdate_number`_x`echo $round_time`_`echo $experiment_number`_own_shedule_random_delay.pcapng
sudo chown mininet:mininet /home/mininet/Documents/mininet_experiment5_`echo $networkupdate_number`_x`echo $round_time`_`echo $experiment_number`_own_shedule_random_delay.pcapng
sudo chmod 777 /home/mininet/Documents/mininet_experiment5_`echo $networkupdate_number`_x`echo $round_time`_`echo $experiment_number`_own_shedule_random_delay.pcapng

sudo tshark -i s1-eth1 -i s1-eth2 -i s1-eth3 -i s1-eth4 -i s2-eth1 -i s2-eth2 -i s2-eth3 -i s2-eth4 -i s3-eth1 -i s3-eth2 -i s3-eth3 -i s3-eth4 -i s4-eth1 -i s4-eth2 -i s4-eth3 -i s4-eth4 -i s5-eth1 -i s5-eth2 -i s5-eth3 -i s5-eth4 -i s6-eth1 -i s6-eth2 -i s6-eth3 -i s6-eth4 -i s7-eth1 -i s7-eth2 -i s7-eth3 -i s7-eth4 -i s8-eth1 -i s8-eth2 -i s8-eth3 -i s8-eth4 -i s9-eth1 -i s9-eth2 -i s9-eth3 -i s9-eth4 -i s10-eth1 -i s10-eth2 -i s10-eth3 -i s10-eth4 -i s11-eth1 -i s11-eth2 -i s11-eth3 -i s11-eth4 -i s12-eth1 -i s12-eth2 -i s12-eth3 -i s12-eth4 -i s13-eth1 -i s13-eth2 -i s13-eth3 -i s13-eth4 -i s14-eth1 -i s14-eth2 -i s14-eth3 -i s14-eth4 -i s15-eth1 -i s15-eth2 -i s15-eth3 -i s15-eth4 -i s16-eth1 -i s16-eth2 -i s16-eth3 -i s16-eth4 -i s17-eth1 -i s17-eth2 -i s17-eth3 -i s17-eth4 -i s18-eth1 -i s18-eth2 -i s18-eth3 -i s18-eth4 -c 20000 -w /home/mininet/Documents/mininet_experiment5_`echo $networkupdate_number`_x`echo $round_time`_`echo $experiment_number`_own_shedule_random_delay.pcapng
