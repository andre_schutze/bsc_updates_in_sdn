echo "RUN SCRIPT: start_wireshark_exp3_01.sh"


experiment_number=$1
networkupdate_number=$2

# tshark: The file "/home/mininet/Documents/mininet_experiment5_01_x0.005_001_own_shedule_random_delay.pcapng" doesn't exist.

sudo touch /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`_own.pcapng
sudo chown mininet:mininet /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`_own.pcapng
sudo chmod 777 /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`_own.pcapng
sudo tshark -i s1-eth2 -i s1-eth3 -i s2-eth2 -i s2-eth4 -i s3-eth2 -i s3-eth4 -i s4-eth1 -i s4-eth3 -i s5-eth2 -i s5-eth3 -i s6-eth2 -i s6-eth4 -i s7-eth1 -i s7-eth3 -i s8-eth1 -i s8-eth2 -i s9-eth2 -i s9-eth3 -i s10-eth2 -i s10-eth4 -i lo -c 20000 -w /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`.pcapng
