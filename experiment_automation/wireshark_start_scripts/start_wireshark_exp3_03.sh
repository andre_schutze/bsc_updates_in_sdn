echo "RUN SCRIPT: start_wireshark_exp3_03.sh"


experiment_number=$1
networkupdate_number=$2

# tshark: The file "/home/mininet/Documents/mininet_experiment5_01_x0.005_001_own_shedule_random_delay.pcapng" doesn't exist.

echo "create file"
sudo touch /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`_own.pcapng
echo "change owner of file"
sudo chown mininet:mininet /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`_own.pcapng
echo "change permissions of file"
sudo chmod 777 /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`_own.pcapng
echo "start wireshark"
sudo tshark -i s3-eth2 -i s3-eth4 -i s7-eth1 -i s7-eth3 -i s8-eth1 -i s8-eth2 -i s9-eth2 -i s9-eth3 -i s10-eth2 -i s10-eth4 -i lo -c 20000 -w /home/mininet/Documents/mininet_experiment3_`echo $networkupdate_number`_`echo $experiment_number`_own.pcapng
echo "wireshark started"
