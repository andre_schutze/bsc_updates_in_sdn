/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traceanalyzer;

/**
 *
 * @author andre
 */
public class Pair<S, T> {
    
    private S s = null;
    private T t = null;
    
    public Pair(S ps, T pt){
        this.s = ps;
        this.t = pt;
    }
    
    public S getKey() {
        return this.s;
    }
    
    public T getValue() {
        return this.t;
    }
}
