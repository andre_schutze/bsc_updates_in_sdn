/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traceanalyzer;

import javax.xml.parsers.*;
import org.xml.sax.*;

import java.util.*;
import java.io.*;
import java.time.LocalTime;

/**
 *
 * @author andre
 */
public class TraceAnalyzer {
    public static String pathToXmlFile = "/home/andre/Dokumente/university/15_SoSe/Bachelorarbeit/traces_for_loop_investigation/mininet_experiment5_01_x0.950_29_own_shedule_random_delay.xml";
    public static String pathToOutFile = "/home/andre/Dokumente/university/15_SoSe/Bachelorarbeit/traces_for_loop_investigation/mininet_experiment5_01_x0.950_29_own_shedule_random_delay_processed.txt";
    public static HashMap<String, HashMap<Pair<Integer, LocalTime>, String>> packets = null;
    public static HashMap<String, Integer> distinguishableRoutes = null;
    
    private static boolean findDuplicates(Collection<String> coll) {
        ArrayList<String> interfaces = new ArrayList<>(coll);
        boolean loop = false;
        for(int i = 0; i<interfaces.size(); i++) {
            String interf = interfaces.get(i);
            for(int j = i+1; j < interfaces.size(); j++) {
                String interf2 = interfaces.get(j);
                if(interf.equals(interf2)) {
                    loop = true;
                }
            }
        }
        return loop;
    }
    
    private static boolean localTimeEqualsLocalTime(LocalTime first, LocalTime second) {
//        System.out.println("first:  " + first.getHour() + ":" + first.getMinute() + ":" + first.getSecond() + "." + first.getNano());
//        System.out.println("second: " + second.getHour() + ":" + second.getMinute() + ":" + second.getSecond() + "." + second.getNano());
        if (first.getHour() == second.getHour() &&
            first.getMinute() == second.getMinute() &&
            first.getSecond() == second.getSecond() &&
            first.getNano() == second.getNano()) {
//            System.out.println("localTimes equal");
            return true;
        }
        return false;
    }
    
    private static void removeFromList(LinkedList<Pair<Integer, LocalTime>> list, Pair<Integer, LocalTime> toRemove) {
        int i = -1;
        boolean found = false;
        for(Pair<Integer, LocalTime> act : list) {
            i++;
//            System.out.println("Integer: " + act.getKey().equals(toRemove.getKey()));
//            System.out.println("LocalTime: " + localTimeEqualsLocalTime(act.getValue(), toRemove.getValue()));
            if(act.getKey().equals(toRemove.getKey()) && localTimeEqualsLocalTime(act.getValue(), toRemove.getValue())) {
                found = true;
                break;
            }
        }
        if(found) {
//            System.out.println("remove");
            list.remove(i);
        }
    }
    
    private static ArrayList<Pair<Integer, LocalTime>> sortList(Collection<Pair<Integer, LocalTime>> unsorted) {
        LinkedList<Pair<Integer, LocalTime>> tmp = new LinkedList<>(unsorted);
        ArrayList<Pair<Integer, LocalTime>> ret = new ArrayList<>();
        Pair<Integer, LocalTime> smallest = null;
        while(!tmp.isEmpty()) {
            smallest = tmp.getFirst();
            for(Pair<Integer, LocalTime> act : tmp) {
                LocalTime actLT = act.getValue();
                LocalTime smallestLT = smallest.getValue();
                if(actLT == null) {
                    int actNum = act.getKey();
                    int smallestNum = smallest.getKey();
                    if(actNum<smallestNum) {
                        smallest = act;
                    }
                } else if(actLT.compareTo(smallestLT) < 0){
                    smallest = act;
                } else if (actLT.compareTo(smallestLT) == 0) {
                    int actNum = act.getKey();
                    int smallestNum = smallest.getKey();
                    if(actNum<smallestNum) {
                        smallest = act;
                    }
                } else {
                    // Do not change smallest
                }
            }
            tmp.remove(smallest);
//            removeFromList(tmp, smallest);
//            System.out.println("tmp.size(): " + tmp.size());
            ret.add(smallest);
//            System.out.println("ret.size(): " + ret.size());
        }
        return ret;
    }
    
    private static void createDistinguishableRoutes(HashMap<Pair<Integer,LocalTime>, String> route, ArrayList<Pair<Integer, LocalTime>> sorted) {
        ArrayList<String> sortedRoute = new ArrayList<String>();
        String key = "";
        
        for(Pair<Integer, LocalTime> act : sorted) {
            String interf = route.get(act);
            sortedRoute.add(interf);
            key = key + interf;
        }
        
        if(distinguishableRoutes.containsKey(key)) {
            Integer num = distinguishableRoutes.get(key);
            num++;
            distinguishableRoutes.put(key, num);
        } else {
            distinguishableRoutes.put(key, 1);
        }
            
        return;
    }
    
    private static void printDistinguisableRoutes() {
        for(String key : distinguishableRoutes.keySet()) {
            Integer num = distinguishableRoutes.get(key);
            
            System.out.println(num + ": " + key);
        }
    }
    
    private static int writePacketsToFile() throws IOException {
        distinguishableRoutes = new HashMap<String, Integer>();
        File outFile = new File(pathToOutFile);
        if(!outFile.exists()) {
            outFile.createNewFile();
        }
        
        PrintWriter writer = new PrintWriter(outFile);
        int loopCount = 0;
//        System.out.println("Packets found in the trace: " + packets.size());
        for(String key : packets.keySet()) {
            if (key == null) {
                continue;
            }
            String s = new String();
            
            HashMap<Pair<Integer,LocalTime>, String> route = packets.get(key);
            Collection<Pair<Integer,LocalTime>> unsorted = route.keySet();
            ArrayList<Pair<Integer, LocalTime>> sorted = sortList(unsorted);
            
            createDistinguishableRoutes(route, sorted);
            
            Collection<String> routeInterfaces = route.values();
            boolean loop = findDuplicates(routeInterfaces);
            
            Pair<Integer, LocalTime> keyRoute = sorted.get(0);
            s = s + route.get(keyRoute);
            for(Pair<Integer, LocalTime> keyRoute2 : sorted) {
                if(!keyRoute.equals(keyRoute2)) {
                    s = s + " - " + route.get(keyRoute2);
                }
            }
            
            if(loop){
                loopCount++;
//                System.out.println("loop in route: " + s);
            }
            
            writer.println(key + ": " + s);
        }
//        System.out.println("loop count: " + loopCount);
        //printDistinguisableRoutes();
        
        return loopCount;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        /*
        if (args.length >0) {
            System.out.println("args[0]=" + args[0]);
        }
        if (args.length >1) {
            System.out.println("args[1]=" + args[1]);
        }
        if (args.length >2) {
            System.out.println("args[2]=" + args[2]);
        }
        if (args.length >3) {
            System.out.println("args[3]=" + args[3]);
        }
        if (args.length >4) {
            System.out.println("args[4]=" + args[4]);
        }
        if (args.length >5) {
            System.out.println("args[5]=" + args[5]);
        }
        if (args.length >6) {
            System.out.println("args[6]=" + args[6]);
        }
                */
        if (args.length < 2) {
            System.out.println("usage:");
            System.out.println("java -jar TraceAnalyser.jar <path-to-xml-file> <path-to-out-file>");
            System.exit(-1);
        }
        
        pathToXmlFile = args[0];
        pathToOutFile = args[1];
        
        // TODO code application logic here
        packets = new HashMap<>();
        
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        SAXParser saxParser = spf.newSAXParser();

        XMLReader xmlReader = saxParser.getXMLReader();
        xmlReader.setContentHandler((ContentHandler) new MyContentHandler(packets));
        xmlReader.parse(pathToXmlFile);
        
//        System.out.println("write packets to file");
        int numOfPacketsWhichLoop = writePacketsToFile();
        System.out.println(numOfPacketsWhichLoop);
    }
    
}
