/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traceanalyzer2;

import javax.xml.parsers.*;
import org.xml.sax.*;

import java.util.*;
import java.io.*;
import java.time.LocalTime;
import javafx.util.Pair;

/**
 *
 * @author andre
 */
public class TraceAnalyzer {
    public static String pathToXmlFile = "/home/andre/Dokumente/university/SoSe_15/Bachelorarbeit/traces_for_loop_investigation/mininet_experiment4_loop_investigation_03_03.xml";
    public static String pathToOutFile = "/home/andre/Dokumente/university/SoSe_15/Bachelorarbeit/traces_for_loop_investigation/mininet_experiment4_loop_investigation_03_03_processed.txt";
    public static HashMap<Integer, Integer> ttls = null;
    
    private static void writeTtlsToConsole() {
        for(Integer key : ttls.keySet()) {
            int val = ttls.get(key);
            System.out.println(key + " - " + val);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        // TODO code application logic here
        ttls = new HashMap<>();
        
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        SAXParser saxParser = spf.newSAXParser();

        XMLReader xmlReader = saxParser.getXMLReader();
        xmlReader.setContentHandler((ContentHandler) new MyContentHandler(ttls));
        xmlReader.parse(pathToXmlFile);
        
        System.out.println("write packets to file");
        writeTtlsToConsole();
    }
    
}
