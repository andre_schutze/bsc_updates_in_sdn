/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traceanalyzer2;

import java.util.HashMap;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 *
 * @author andre
 */
public class MyContentHandler implements ContentHandler{
    private Locator locator = null;
    private HashMap<Integer, Integer> ttls = null;
    
    public MyContentHandler(HashMap<Integer, Integer> ttls) {
        this.ttls = ttls;
    }
    
    @Override
    public void setDocumentLocator(Locator locator) {
        this.locator = locator;
    }

    @Override
    public void startDocument() throws SAXException {
        System.out.println("start Document");
    }

    @Override
    public void endDocument() {
        System.out.println("end Document");
    }

    @Override
    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void endPrefixMapping(String prefix) throws SAXException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) {
        for (int i = 0; atts.getLocalName(i) != null; i++) {
            if (atts.getValue(i).equals("ip.ttl")) {
                String s = atts.getValue(i+2);
//                System.out.println("i+1: " + atts.getValue(i+1) + " i+2: " + atts.getValue(i+2) + " i+3: " + atts.getValue(i+3) + " i+4: " + atts.getValue(i+4) + " i+5: " + atts.getValue(i+5));
                System.out.println("ip.ttl: " + s);
//                s = s.split(" ")[3];
                if(ttls.containsKey(Integer.parseInt(s))) {
                    int val = ttls.get(Integer.parseInt(s));
                    val++;
                    ttls.put(Integer.parseInt(s), val);
                } else {
                    ttls.put(Integer.parseInt(s), 1);
                }
            }
/*            
            if (atts.getValue(i).equals("frame.time")) {
                String s = atts.getValue(i+1);
//                System.out.println("Time: " + s);
                String[] strings = s.split(" ");
//                System.out.println("Time: " + strings[5]);
                timestamp = LocalTime.parse(strings[5]);
            }
            
            if (atts.getValue(i).equals("frame.interface_id")) {
                String s = atts.getValue(i+1);
//              System.out.println("interface: " + s);
                interf = s.split(" ")[3];
            }
            
            if (atts.getValue(i).equals("data")) {
                data = atts.getValue(i+1);
//                System.out.println("Data: " + s);
            }

            if (atts.getValue(i).equals("tcp.seq")) {
                String s = atts.getValue(i+1);
                seq = s.split(" ")[2];
                
//                if(seq == 1) {
//                    System.out.println("sequence-nr: " + seq + " on interface: " + interf);
//                }
            }

            if (atts.getValue(i).equals("tcp.stream")) {
                String s = atts.getValue(i+1);
                stream = s.split(" ")[2];
                
//                if(seq == 1) {
//                    System.out.println("sequence-nr: " + seq + " on interface: " + interf);
//                }
            }
            
            if (atts.getValue(i).equals("tcp.ack")) {
                String s = atts.getValue(i+1);
                String ack = s.split(" ")[2];
//                System.out.println("ack-nr: " + ack + " on interface: " + interf);
                
//                String key = seq + ack + stream + data;
                String key = data;
                
                if (packets.containsKey(key)) {
                    HashMap<Pair<Integer, LocalTime>, String> routePoints = packets.get(key);
                    routePoints.put(new Pair(num, timestamp), interf);
                } else {
                    HashMap<Pair<Integer, LocalTime>, String> routePoints = new HashMap<>();
                    routePoints.put(new Pair(num, timestamp), interf);
                    packets.put(key, routePoints);
                }
            
            }
            */
        }
        
        
        
/*
        boolean print = false;
        for (int i = 0; atts.getLocalName(i) != null; i++) {
            //System.out.println(atts.getValue(i));
            if (atts.getValue(i).equals("frame.number") || atts.getValue(i).equals("tcp.seq") || atts.getValue(i).equals("frame.interface_id")) {
                print = true;
            }
            if(print) {
                System.out.println("LocalName: " + atts.getLocalName(i) + " getType: " + atts.getType(i) + " Uri: " + atts.getURI(i) + " Value: " + atts.getValue(i));
            }
        }
*/
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
//        System.out.println("endElement: Uri: " + uri + " LocalName: " + localName + " qName: " + qName);
//        System.out.println("-----------------------------------------------");
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        
//        System.out.println("characters: Characters: " + new String(ch) + " Start: " + start + " Length: " + length);
    }

    @Override
    public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void processingInstruction(String target, String data) throws SAXException {
        System.out.println("processingInstruction: Target: " + target + " Data: " + data);
    }

    @Override
    public void skippedEntity(String name) throws SAXException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
