/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traceanalyzer_utils;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 *
 * @author andre
 */
public class MyContentHandler_StartAndEndOfFlow implements ContentHandler{
    private Locator locator = null;
    
    private Trace trace = null;
    
    private String interf = null;
    private double timeOffset = -1000.0;
    
    private boolean restRequest = false;
    private boolean barrierReply = false;
/*    
    private int num = 0;
    private String interf = null;
    private LocalTime timestamp = null;
    private String data = null;
    private String stream = null;
    private String seq = null;
    private boolean spurious_retransmission = false;
*/    
    public MyContentHandler_StartAndEndOfFlow(Trace trace) {
        this.trace = trace;
    }
    
    @Override
    public void setDocumentLocator(Locator locator) {
        this.locator = locator;
    }

    @Override
    public void startDocument() throws SAXException {
//        System.out.println("start Document");
    }

    @Override
    public void endDocument() {
//        System.out.println("end Document");
    }

    @Override
    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void endPrefixMapping(String prefix) throws SAXException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) {
        for (int i = 0; atts.getLocalName(i) != null; i++) {
/*
            if (atts.getValue(i).equals("num")) {
                String s = atts.getValue(i+2);
//                System.out.println("num: " + s);
                num = Integer.parseInt(s);
            }
            
            if (atts.getValue(i).equals("frame.time")) {
                String s = atts.getValue(i+1);
//                System.out.println("Time: " + s);
                String[] strings = s.split(" ");
//                System.out.println("Time: " + strings[5]);
                if(strings[5].equals("2015") || strings[5].equals("2016")) {
                    timestamp = LocalTime.parse(strings[6]);
                } else {
                    timestamp = LocalTime.parse(strings[5]);
                }
            }
  */          
            if (atts.getValue(i).equals("frame.interface_id")) {
                String s = atts.getValue(i+1);
//              System.out.println("interface: " + s);
                try{
                    interf = s.split(" ")[2];
                } catch(ArrayIndexOutOfBoundsException e) {
                    System.err.println("Error during parsing pdml: interface can't retrieved >" + s + "<" );
                }
            }
/*
            if (atts.getValue(i).equals("tcp.seq")) {
                String s = atts.getValue(i+1);
                seq = s.split(" ")[2];
                
//                if(seq == 1) {
//                    System.out.println("sequence-nr: " + seq + " on interface: " + interf);
//                }
            }

            if (atts.getValue(i).equals("tcp.stream")) {
                String s = atts.getValue(i+1);
                stream = s.split(" ")[2];
                
//                if(seq == 1) {
//                    System.out.println("sequence-nr: " + seq + " on interface: " + interf);
//                }
            }
            
            if (atts.getValue(i).equals("tcp.ack")) {
                String s = atts.getValue(i+1);
                String ack = s.split(" ")[2];
//                System.out.println("ack-nr: " + ack + " on interface: " + interf);
            }
            
            if (atts.getValue(i).equals("tcp.analysis.spurious_retransmission")) {
//                String s = atts.getValue(i+1);
//                String ack = s.split(" ")[2];
                spurious_retransmission = true;
//                System.out.println("ack-nr: " + ack + " on interface: " + interf);
            }
*/            
            if (atts.getValue(i).equals("frame.time_relative")) {
                String tmpTimeOffset = atts.getValue(i+1);
                String timeAsString = tmpTimeOffset.split(" ")[6];
//                System.out.println("time: " + timeAsString);
                try{
                    timeOffset = Double.parseDouble(timeAsString);
                } catch (Exception e) {
                    System.err.println("Error during parsing pdml: timeOffset can't retrieved >" + timeAsString + "<" );
                }
            }
            
            
//          REST request found  
            if (atts.getValue(i).contains("http.request.uri")) {
//                System.out.println("found http.request.uri");
//                System.out.println(atts.getValue(i));
                if (atts.getValue(i+1).contains("flowupdate")) {
//                    System.out.println("rest message found: " + timeOffset);
                    trace.setRestMessageReceiveTime(timeOffset);
                    continue;
                }
            }
            
            if (atts.getValue(i).equals("of10.barrier_reply.type")) {
//                System.out.println("found barrier reply: " + timeOffset);
                barrierReply = true;
                trace.addTime(timeOffset);
                continue;
            }
            
            if (atts.getValue(i).equals("data")) {
                if(barrierReply) {
                    trace.addTime(timeOffset);
                }
                barrierReply = false;
                interf = null;
                timeOffset = -1000.0;
                restRequest = false;
            }
        }
        
        
        
/*
        boolean print = false;
        for (int i = 0; atts.getLocalName(i) != null; i++) {
            //System.out.println(atts.getValue(i));
            if (atts.getValue(i).equals("frame.number") || atts.getValue(i).equals("tcp.seq") || atts.getValue(i).equals("frame.interface_id")) {
                print = true;
            }
            if(print) {
                System.out.println("LocalName: " + atts.getLocalName(i) + " getType: " + atts.getType(i) + " Uri: " + atts.getURI(i) + " Value: " + atts.getValue(i));
            }
        }
*/
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
//        System.out.println("endElement: Uri: " + uri + " LocalName: " + localName + " qName: " + qName);
//        System.out.println("-----------------------------------------------");
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        
//        System.out.println("characters: Characters: " + new String(ch) + " Start: " + start + " Length: " + length);
    }

    @Override
    public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void processingInstruction(String target, String data) throws SAXException {
//        System.out.println("processingInstruction: Target: " + target + " Data: " + data);
    }

    @Override
    public void skippedEntity(String name) throws SAXException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
