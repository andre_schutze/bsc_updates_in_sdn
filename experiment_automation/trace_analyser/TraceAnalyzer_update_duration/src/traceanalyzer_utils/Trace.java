/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traceanalyzer_utils;

import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class Trace {
    private ArrayList<Trace_interface> interfaces = null;
    private double restMessageReceive = -1000.0;
    
    public Trace(){
        interfaces = new ArrayList<Trace_interface>();
        restMessageReceive = -1000.0;
    }
    
    public void addTime(String interface_name, double time) {
//        System.out.println(interface_name + " " + time);
        for(Trace_interface act : interfaces) {
            if(act.getName().equals(interface_name)) {
                act.setTime(time);
                return;
            }
        }
        interfaces.add(new Trace_interface(interface_name, time, time));
//        System.out.println("interface added");
    }
    
    public void setRestMessageReceiveTime(double time) {
//        System.out.println("rest message recieve: " + time);
        restMessageReceive = time;
    }
    
    public double getUpdateDuration() {

        ArrayList<Double> eventTimes = new ArrayList<Double>();
        
        for(Trace_interface act : interfaces) {
            if(act.getStart() > restMessageReceive && act.getStart() < (restMessageReceive+0.5)) {
//                System.out.println("event added: start");
                eventTimes.add(act.getStart());
            }
            if(act.getEnd() > restMessageReceive && act.getEnd() < (restMessageReceive+0.5)) {
//                System.out.println("event added: end");
                eventTimes.add(act.getEnd());
            }
//            System.out.println(act.getStart() + " " + act.getEnd());
        }
        
        double latest = restMessageReceive;
        for(Double time : eventTimes) {
//            System.out.println("time of event is: " + time);
            if(time > latest) {
                latest = time;
            }
        }
        
        return latest - restMessageReceive;
    }
}
