/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traceanalyzer_utils;

/**
 *
 * @author andre
 */
public class Trace_interface {
    private String name = null;
    private double start = -1000.0;
    private double end = 1000.0;
    
    public Trace_interface() {
        name = null;
        start = -1000.0;
        end = 1000.0;
    }
    
    public Trace_interface(String name, double start, double end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }
    
    public String getName() {
        return this.name;
    }
    
    public double getStart() {
        return this.start;
    }
    
    public double getEnd() {
        return this.end;
    }
/*    
    public void setName(String name){
        this.name = name;
    }
    
    public void setStart(double start){
        this.start = start;
    }
    
    public void setEnd(double end){
        this.end = end;
    }
*/
    public void setTime(double time) {
        if (time < start) {
            start = time;
        }
        if (time > end) {
            end = time;
        }
    }
}
