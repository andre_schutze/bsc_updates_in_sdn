/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traceanalyzer_update_duration;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import traceanalyzer_utils.MyContentHandler_StartAndEndOfFlow;
import traceanalyzer_utils.Trace;

/**
 *
 * @author andre
 */
public class TraceAnalyzer_update_duration {
    private static String pathToXmlFile = null;
    private static String pathToOutFile = null;
    private static Trace trace = null;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        if (args.length < 2) {
            System.out.println("usage:");
            System.out.println("java -jar TraceAnalyser_update_duration.jar <path-to-xml-file> <path-to-out-file>");
            System.exit(-1);
        }
        
        pathToXmlFile = args[0];
        pathToOutFile = args[1];
        
        trace = new Trace();
        
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        SAXParser saxParser = spf.newSAXParser();
        
        XMLReader xmlReader = saxParser.getXMLReader();
        xmlReader.setContentHandler((ContentHandler) new MyContentHandler_StartAndEndOfFlow(trace));
        xmlReader.parse(pathToXmlFile);
        
        System.out.println(trace.getUpdateDuration());
    }
    
}
