#!/bin/bash

function formatIntegerToFixedLengthString {
    # $1 ... number to format
    # $2 ... length of string
    printf "%0*d" $2 $1
}

function formatExpNum {
    # $1 ... experiment number
    formatIntegerToFixedLengthString $1 3
}

function formatUpdNum {
    # $1 ... update number
    formatIntegerToFixedLengthString $1 2
}

function executeExperiment {
    # $1 ... networkUpdateNumber
    # $2 ... roundTime
    # $3 ... experimentNumber

networkUpdateNumber=$1

roundTime=$2
pathToLoopNumberOutputFile="/home/mininet/Documents/networkupdate-`formatUpdNum $networkUpdateNumber`_roundTime-`echo $roundTime`.txt"

experimentNumber=$3
        


            pathToMininetScript="/home/mininet/Documents/own_code/topo_experiment5_ownShedule_`formatUpdNum $networkUpdateNumber`.py `formatExpNum $experimentNumber` `echo $roundTime` `formatUpdNum $networkUpdateNumber`"
            pathToPcapngFile="/home/mininet/Documents/mininet_experiment5_`formatUpdNum $networkUpdateNumber`_x`echo $roundTime`_`formatExpNum $experimentNumber`_own_shedule_random_delay.pcapng"
            pathToXmlFile="/home/mininet/Documents/mininet_experiment5_`formatUpdNum $networkUpdateNumber`_x`echo $roundTime`_`formatExpNum $experimentNumber`_own_shedule_random_delay.xml"
            pathToOutFile="/home/mininet/Documents/mininet_experiment5_`formatUpdNum $networkUpdateNumber`_x`echo $roundTime`_`formatExpNum $experimentNumber`_own_shedule_random_delay_processed.txt"


            echo "clearup mininet"
            sudo mn -c
            echo "start experiment in mininet"
            sudo python `echo $pathToMininetScript`


            echo "start to convert pcapng - file to pdml - file"
            tshark -r `echo $pathToPcapngFile` -T pdml > `echo $pathToXmlFile`

            echo "analyse pdml file to determine the number of packets which loop"
            echo "read file `echo $pathToOutFile`"
            echo "write to file `echo ${pathToLoopNumberOutputFile}`"
            java -jar /home/mininet/Documents/TraceAnalyzer.jar `echo $pathToXmlFile` `echo $pathToOutFile` >> "`echo ${pathToLoopNumberOutputFile}`"

#            num_of_loops=$?
#
#            echo "trace analysation finished. `echo $num_of_loops` loops detected"
#            echo $num_of_loops >> $pathToLoopNumberOutputFile

            echo "clean up"
            rm $pathToPcapngFile
            rm $pathToXmlFile
            rm $pathToOutFile

}








# anzahlNetzwerkUpdates = 4
# anzahlRoundTimes = 30
# anzahlExperiments=100
# Gesamt = 8,3333 Tage


#roundTimesOfRoundFour=( "1.200" "1.300")


#roundTimesOfRoundTwo=( "0.850" "0.900" "1.000" "1.100" "1.200" "1.300")

#roundTimesOfRoundThree=( "0.900" "1.000" "1.100" "1.200" "1.300")

roundTimesOfRoundFour=( "1.100" "1.200" "1.300") 


roundTimes=( "0.005" "0.010" "0.015" "0.025" "0.035" "0.050" "0.075" "0.100" "0.125" "0.150" "0.175" "0.200" "0.250" "0.300" "0.350" "0.400" "0.450" "0.500" "0.550" "0.600" "0.650" "0.700" "0.750" "0.800" "0.850" "0.900" "1.000" "1.100" "1.200" "1.300") 









networkUpdateNumber=4
roundTime="0.900"

        for experimentNumber in {69..100};
        do

	    executeExperiment $networkUpdateNumber $roundTime $experimentNumber
        done


#networkUpdateNumber=4
#roundTime="1.000"
#
#        for experimentNumber in {64..100};
#        do
#
#            executeExperiment $networkUpdateNumber $roundTime $experimentNumber
#        done

#
#networkUpdateNumber=3
#roundTime="1.300"
#
#        for experimentNumber in {1..100};
#        do
#
#            executeExperiment $networkUpdateNumber $roundTime $experimentNumber
#        done


#networkUpdateNumber=3
#    for roundTime in "${roundTimesOfRoundThree[@]}"
#    do
#        echo "create loop number output file"
#        pathToLoopNumberOutputFile="/home/mininet/Documents/networkupdate-`formatUpdNum $networkUpdateNumber`_roundTime-`echo $roundTime`.txt"
#        touch $pathToLoopNumberOutputFile
#        for experimentNumber in {1..100};
#        do
#
#            executeExperiment $networkUpdateNumber $roundTime $experimentNumber
#        done
#    done



#networkUpdateNumber=3
#roundTime="0.025"
#
#        for experimentNumber in {36..100};
#        do
#
#            executeExperiment $networkUpdateNumber $roundTime $experimentNumber
#        done
#
networkUpdateNumber=4
    for roundTime in "${roundTimesOfRoundFour[@]}"
    do
        echo "create loop number output file"
        pathToLoopNumberOutputFile="/home/mininet/Documents/networkupdate-`formatUpdNum $networkUpdateNumber`_roundTime-`echo $roundTime`.txt"
        touch $pathToLoopNumberOutputFile
        for experimentNumber in {1..100};
        do

            executeExperiment $networkUpdateNumber $roundTime $experimentNumber
        done
    done



#    for roundTime in "${roundTimesOfRoundOne[@]}"
#    do
#        echo "create loop number output file"
#        pathToLoopNumberOutputFile="/home/mininet/Documents/networkupdate-`formatUpdNum $networkUpdateNumber`_roundTime-`echo $roundTime`.txt"
#        touch $pathToLoopNumberOutputFile
#        for experimentNumber in {1..100};
#        do
#
#            executeExperiment $networkUpdateNumber $roundTime $experimentNumber
#        done
#    done






##for networkUpdateNumber in {3..4};
##do
#    for roundTime in "${roundTimesOfRoundFour[@]}"
#    do
#        echo "create loop number output file"
#        pathToLoopNumberOutputFile="/home/mininet/Documents/networkupdate-`formatUpdNum $networkUpdateNumber`_roundTime-`echo $roundTime`.txt"
#        touch $pathToLoopNumberOutputFile
##        sudo chown mininet:mininet $pathToLoopNumberOutputFile
##        sudo chmod 777 $pathToLoopNumberOutputFile
#
#        for experimentNumber in {1..100};
#        do


#        done
#    done
#done
