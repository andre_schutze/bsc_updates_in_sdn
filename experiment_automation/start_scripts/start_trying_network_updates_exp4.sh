#!/bin/bash

function formatIntegerToFixedLengthString {
    # $1 ... number to format
    # $2 ... length of string
    printf "%0*d" $2 $1
}

function formatExpNum {
    # $1 ... experiment number
    formatIntegerToFixedLengthString $1 3
}

function formatUpdNum {
    # $1 ... update number
    formatIntegerToFixedLengthString $1 2
}

function executeExperiment {
    # $1 ... networkUpdateNumber
    # $2 ... experimentNumber

networkUpdateNumber=$1
experimentNumber=$2
pathToLoopNumberOutputFile="/home/mininet/Documents/networkupdate_exp4_`formatUpdNum $networkUpdateNumber`.txt"
        


            pathToMininetScript="/home/mininet/Documents/own_code/topo_experiment4_update_peacock_`formatUpdNum $networkUpdateNumber`_automated.py `formatExpNum $experimentNumber` `formatUpdNum $networkUpdateNumber`"
            pathToPcapngFile="/home/mininet/Documents/mininet_experiment4_`formatUpdNum $networkUpdateNumber`_`formatExpNum $experimentNumber`_own.pcapng"
            pathToXmlFile="/home/mininet/Documents/mininet_experiment4_`formatUpdNum $networkUpdateNumber`_`formatExpNum $experimentNumber`_own.xml"
            pathToOutFile="/home/mininet/Documents/mininet_experiment4_`formatUpdNum $networkUpdateNumber`_`formatExpNum $experimentNumber`_own.txt"


            echo "clearup mininet"
            sudo mn -c
            echo "start experiment in mininet"
            sudo python `echo $pathToMininetScript`


            echo "start to convert pcapng - file to pdml - file"
            tshark -r `echo $pathToPcapngFile` -T pdml > `echo $pathToXmlFile`

            echo "analyse pdml file to determine the number of packets which loop"
            echo "read file `echo $pathToOutFile`"
            echo "write to file `echo ${pathToLoopNumberOutputFile}`"
#            java -jar /home/mininet/Documents/TraceAnalyzer_update_duration.jar `echo $pathToXmlFile` `echo $pathToOutFile` >> "`echo ${pathToLoopNumberOutputFile}`"
            java -jar /home/mininet/Documents/TraceAnalyzer_update_duration_lo.jar `echo $pathToXmlFile` `echo $pathToOutFile` >> "`echo ${pathToLoopNumberOutputFile}`"

	    sudo kill pidof tshark
#            num_of_loops=$?
#
#            echo "trace analysation finished. `echo $num_of_loops` loops detected"
#            echo $num_of_loops >> $pathToLoopNumberOutputFile

            echo "clean up"
            rm $pathToPcapngFile
            rm $pathToXmlFile
            rm $pathToOutFile

}








# anzahlExperiments=100

networkUpdateNumber=3

pathToLoopNumberOutputFile="/home/mininet/Documents/networkupdate_exp4_`formatUpdNum $networkUpdateNumber`.txt"
touch `echo $pathToLoopNumberOutputFile`
chmod 777 `echo $pathToLoopNumberOutputFile`
chown mininet:mininet `echo $pathToLoopNumberOutputFile`

        for experimentNumber in {1..500};
        do

	    executeExperiment $networkUpdateNumber $experimentNumber
        done




networkUpdateNumber=5

pathToLoopNumberOutputFile="/home/mininet/Documents/networkupdate_exp4_`formatUpdNum $networkUpdateNumber`.txt"
touch `echo $pathToLoopNumberOutputFile`
chmod 777 `echo $pathToLoopNumberOutputFile`
chown mininet:mininet `echo $pathToLoopNumberOutputFile`

        for experimentNumber in {1..500};
        do

            executeExperiment $networkUpdateNumber $experimentNumber
        done

