curl -X POST -d '{
    "dpid": 1,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 3600,
    "hard_timeout": 3600,
    "priority": 0,
    "flags": 1,
    "match":{
        "in_port": 1
    },
    "actions":[
        {
            "type":"DEC_NW_TTL"
        }
    ]
 }' http://localhost:8080/stats/flowentry/add
