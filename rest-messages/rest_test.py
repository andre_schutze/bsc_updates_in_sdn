curl -X POST -d '{
    "dpid": 1,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 300,
    "hard_timeout": 300,
    "priority": 11111,
    "flags": 1,
    "match":{
	"nw_dst": "10.0.0.3",
	"dl_type": "2048"
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 5
        }
    ]
 }' http://localhost:8080/stats/flowentry/add

curl -X POST -d '{
    "dpid": 1,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 300,
    "hard_timeout": 300,
    "priority": 11111,
    "flags": 1,
    "match":{
        "nw_dst": "10.0.0.1",
	"dl_type": "2048"
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 3
        }
    ]
 }' http://localhost:8080/stats/flowentry/add
