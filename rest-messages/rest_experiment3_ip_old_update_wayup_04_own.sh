curl -X POST -d '{
    "oldpath":{1,3,2,4,6],
    "newpath":[1,4,3,2,5,6],
    "wp":3,
    "interval":10,
    "modify":[{
        "dpid": 1,
        "cookie": 1,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
	    "nw_dst":"10.0.0.2",
	    "dl_type": 2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 3
            }
        ]
    }],
    "modify":[{
        "dpid": 4,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 3,
        "cookie": 1,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 2
            }
        ]
    }],
    "modify":[{
        "dpid": 2,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 3
            }
        ]
    }],
    "add":[{
        "dpid": 5,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 2
            }
        ]
    }],
 }' http://localhost:8080/stats/flowupdate

