curl -X POST -d '{
    "dpid": 1,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 3600,
    "hard_timeout": 3600,
    "priority": 0,
    "flags": 1,
    "match":{
        "in_port": 1
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 4
        }
    ]
 }' http://localhost:8080/stats/flowentry/add

curl -X POST -d '{
    "dpid": 1,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 3600,
    "hard_timeout": 3600,
    "priority": 0,
    "flags": 1,
    "match":{
        "in_port": 4
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 1
        }
    ]
 }' http://localhost:8080/stats/flowentry/add

curl -X POST -d '{
    "dpid": 4,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 3600,
    "hard_timeout": 3600,
    "priority": 0,
    "flags": 1,
    "match":{
        "in_port": 4
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 2
        }
    ]
 }' http://localhost:8080/stats/flowentry/add

curl -X POST -d '{
    "dpid": 4,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 3600,
    "hard_timeout": 3600,
    "priority": 0,
    "flags": 1,
    "match":{
        "in_port": 2
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 4
        }
    ]
 }' http://localhost:8080/stats/flowentry/add

curl -X POST -d '{
    "dpid": 1,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 300,
    "hard_timeout": 300,
    "priority": 11111,
    "flags": 1,
    "match":{
	"nw_dst":"10.0.0.2",
	"dl_type":2048
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 2
        }
    ]
 }' http://localhost:8080/stats/flowentry/add

curl -X POST -d '{
    "dpid": 1,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 300,
    "hard_timeout": 300,
    "priority": 11111,
    "flags": 1,
    "match":{
        "nw_dst":"10.0.0.1",
	"dl_type":2048
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 1
        }
    ]
 }' http://localhost:8080/stats/flowentry/add



curl -X POST -d '{
    "dpid": 2,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 300,
    "hard_timeout": 300,
    "priority": 11111,
    "flags": 1,
    "match":{
        "nw_dst":"10.0.0.2",
	"dl_type":2048
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 2
        }
    ]
 }' http://localhost:8080/stats/flowentry/add

curl -X POST -d '{
    "dpid": 3,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 300,
    "hard_timeout": 300,
    "priority": 11111,
    "flags": 1,
    "match":{
        "nw_dst":"10.0.0.2",
	"dl_type":2048
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 2
        }
    ]
 }' http://localhost:8080/stats/flowentry/add

curl -X POST -d '{
    "dpid": 4,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 300,
    "hard_timeout": 300,
    "priority": 11111,
    "flags": 1,
    "match":{
        "nw_dst":"10.0.0.2",
        "dl_type":2048
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 2
        }
    ]
 }' http://localhost:8080/stats/flowentry/add
