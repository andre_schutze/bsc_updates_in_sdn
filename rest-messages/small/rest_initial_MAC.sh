curl -X POST -d '{
    "dpid": 1,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 3600,
    "hard_timeout": 3600,
    "priority": 0,
    "flags": 1,
    "match":{
	"dl_dst": "ff:ff:ff:ff:ff:ff"
    },
    "actions":[
        {
            "type":"OUTPUT",
	    "port": 2
        }
    ]
 }' http://localhost:8080/stats/flowentry/add

curl -X POST -d '{
    "dpid": 1,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 300,
    "hard_timeout": 300,
    "priority": 11111,
    "flags": 1,
    "match":{
	"dl_dst": "76:e4:5d:75:fe:4c"
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 1
        }
    ]
 }' http://localhost:8080/stats/flowentry/add

curl -X POST -d '{
    "dpid": 1,
    "cookie": 1,
    "cookie_mask": 1,
    "table_id": 0,
    "idle_timeout": 300,
    "hard_timeout": 300,
    "priority": 11111,
    "flags": 1,
    "match":{
	"dl_dst": "b2:43:59:04:f0:54"
    },
    "actions":[
        {
            "type":"OUTPUT",
            "port": 2
        }
    ]
 }' http://localhost:8080/stats/flowentry/add
