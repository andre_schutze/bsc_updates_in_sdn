curl -X POST -d '{
    "first":[1,3],
    "second":[2],
    "third":[],
    "fourth":[],
    "fifth":[],
    "modify":[{
        "dpid": 1,
        "cookie": 1,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
	    "nw_dst":"10.0.0.2",
	    "dl_type": 2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 3
            }
        ]
    }],
    "modify":[{
        "dpid": 2,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 3
            }
        ]
    }],
    "modify":[{
        "dpid": 3,
        "cookie": 1,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
 }' http://localhost:8080/stats/flowupdate_own_shedule

