curl -X POST -d '{
    "oldpath":{1,2,3,4,5,6,7,8,9,10,11,12],
    "newpath":[1,5,3,2,6,4,9,8,7,10,12],
    "modify":[{
        "dpid": 1,
        "cookie": 1,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "in_port":3
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 3
            }
        ]
    }],
    "add":[{
        "dpid": 5,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "in_port":3
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "add":[{
        "dpid": 3,
        "cookie": 1,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "in_port":4
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 3
            }
        ]
    }],
    "add":[{
        "dpid": 2,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "in_port":4
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 3
            }
        ]
    }],
    "add":[{
        "dpid": 6,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "in_port":3
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "add":[{
        "dpid": 4,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "in_port":4
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 3
            }
        ]
    }],
    "add":[{
        "dpid": 9,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "in_port":3
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 1
            }
        ]
    }],
    "add":[{
        "dpid": 8,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "in_port":2
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 1
            }
        ]
    }],
    "add":[{
        "dpid": 7,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "in_port":2
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 3
            }
        ]
    }],
    "add":[{
        "dpid": 10,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "in_port":3
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "add":[{
        "dpid": 12,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "in_port":3
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 2
            }
        ]
    }],
 }' http://localhost:8080/stats/flowupdate_peacock

