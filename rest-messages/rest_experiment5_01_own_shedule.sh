curl -X POST -d '{
    "first":[1],
    "second":[16,15,14,13,12,11,10,9,8,7,6,5,4,3,2],
    "third":[17],
    "fourth":[],
    "fifth":[],
    "modify":[{
        "dpid": 1,
        "cookie": 1,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
	    "nw_dst":"10.0.0.2",
	    "dl_type": 2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 3
            }
        ]
    }],
    "modify":[{
        "dpid": 17,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 16,
        "cookie": 1,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 15,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 14,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 13,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 12,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 11,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 10,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 9,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 8,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 7,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 6,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 5,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 4,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 3,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 4
            }
        ]
    }],
    "modify":[{
        "dpid": 2,
        "cookie_mask": 1,
        "table_id": 0,
        "idle_timeout": 300,
        "hard_timeout": 300,
        "priority": 11112,
        "flags": 1,
        "match":{
            "nw_dst":"10.0.0.2",
            "dl_type":2048
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port": 3
            }
        ]
    }],
 }' http://localhost:8080/stats/flowupdate_own_shedule

