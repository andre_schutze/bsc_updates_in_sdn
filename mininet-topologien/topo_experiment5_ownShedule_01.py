#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController
from mininet.cli import CLI
from mininet.link import TCLink
from mininet.log import setLogLevel, info

import os, time, fcntl, Queue
import sys, subprocess, datetime
from threading import Thread
from thread import start_new_thread

if len(sys.argv) < 4:
    print("usage: python <script_name> <experiment_number> <round-time> <networkupdate-number>")
    sys.exit()


experiment_number = sys.argv[1]
round_time = sys.argv[2]
networkupdate_number = sys.argv[3]


def enqueue_output(out, queue):
    for line in iter(out.readline, b''):
        queue.put(line)
    out.close()

def enqueue_error(err, queue):
    for line in iter(err.readline, b''):
        queue.put(line)
    err.close()

def startLoadGeneratorServerOnH2(pHost):
    pHost.cmd( 'java -jar /home/mininet/Downloads/LoadGeneratorServer.jar' )

def startLoadGeneratorClientOnH1(pHost):
    pHost.cmd( 'java -jar -Xmx1024m /home/mininet/Downloads/LoadGeneratorClient.jar 10.0.0.2' )

def startWireshark():
    os.system( '/home/mininet/start_wireshark2.sh ' + str(sys.argv[1]) )

def myNet():


    #OpenDayLight controller
#    ODL_CONTROLLER_IP='10.0.0.4'

    #Floodlight controller
#    FL_CONTROLLER_IP='10.0.0.5'

    net = Mininet( topo=None, build=False, link=TCLink)

    # Create nodes
    h1 = net.addHost( 'h1' )
#    h1.setMac("00:00:00:00:00:01", "eth0")
    h2 = net.addHost( 'h2' )
#    h2.setMac("00:00:00:00:01:01", "eth0")

    # Create switches
    s1 = net.addSwitch( 's1' )
    s2 = net.addSwitch( 's2' )
    s3 = net.addSwitch( 's3' )
    s4 = net.addSwitch( 's4' )
    s5 = net.addSwitch( 's5' )
    s6 = net.addSwitch( 's6' )
    s7 = net.addSwitch( 's7' )
    s8 = net.addSwitch( 's8' )
    s9 = net.addSwitch( 's9' )
    s10 = net.addSwitch( 's10' )
    s11 = net.addSwitch( 's11' )
    s12 = net.addSwitch( 's12' )
    s13 = net.addSwitch( 's13' )
    s14 = net.addSwitch( 's14' )
    s15 = net.addSwitch( 's15' )
    s16 = net.addSwitch( 's16' )
    s17 = net.addSwitch( 's17' )
    s18 = net.addSwitch( 's18' )
    
    print "*** Creating links"
    net.addLink(h1, s1, port1=1, port2=1, bw=1, delay='5ms')
    net.addLink(s18, h2, port1=2, port2=1, bw=1, delay='5ms')

    net.addLink(s1, s18, port1=4, port2=4, bw=1, delay='5ms')
   
    net.addLink(s1, s2, port1=2, port2=1, bw=1, delay='5ms')  
    net.addLink(s2, s3, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s3, s4, port1=2, port2=1, bw=1, delay='5ms')  
    net.addLink(s4, s5, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s5, s6, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s6, s7, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s7, s8, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s8, s9, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s9, s10, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s10, s11, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s11, s12, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s12, s13, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s13, s14, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s14, s15, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s15, s16, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s16, s17, port1=2, port2=1, bw=1, delay='5ms')
    net.addLink(s17, s18, port1=2, port2=1, bw=1, delay='5ms')

    net.addLink(s1, s17, port1=3, port2=3, bw=1, delay='5ms')  
    net.addLink(s2, s18, port1=3, port2=3, bw=1, delay='5ms')  

    net.addLink(s17, s16, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s16, s15, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s15, s14, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s14, s13, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s13, s12, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s12, s11, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s11, s10, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s10, s9, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s9, s8, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s8, s7, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s7, s6, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s6, s5, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s5, s4, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s4, s3, port1=4, port2=3, bw=1, delay='5ms')
    net.addLink(s3, s2, port1=4, port2=4, bw=1, delay='5ms')

    # Add Controllers
    ryu_ctrl_rest = net.addController( 'c0', controller=RemoteController, ip='127.0.0.1', port=6633)

#    fl_ctrl = net.addController( 'c1', controller=RemoteController, ip=FL_CONTROLLER_IP, port=6633)


    net.build()

    # Connect each switch to the controller
    s1.start( [ryu_ctrl_rest] )
    s2.start( [ryu_ctrl_rest] )
    s3.start( [ryu_ctrl_rest] )
    s4.start( [ryu_ctrl_rest] )
    s5.start( [ryu_ctrl_rest] )
    s6.start( [ryu_ctrl_rest] )
    s7.start( [ryu_ctrl_rest] )
    s8.start( [ryu_ctrl_rest] )
    s9.start( [ryu_ctrl_rest] )
    s10.start( [ryu_ctrl_rest] )
    s11.start( [ryu_ctrl_rest] )
    s12.start( [ryu_ctrl_rest] )
    s13.start( [ryu_ctrl_rest] )
    s14.start( [ryu_ctrl_rest] )
    s15.start( [ryu_ctrl_rest] )
    s16.start( [ryu_ctrl_rest] )
    s17.start( [ryu_ctrl_rest] )
    s18.start( [ryu_ctrl_rest] )


##    s1.cmdPrint('ovs-vsctl show')
#
#    CLI( net )
##    net.stop()


# experiment_number = sys.argv[1]
# round_time = sys.argv[2]
# networkupdate_number = sys.argv[3]


    print("")
    print("> send rest requests to to the controller to initialize a route")
    os.system( '/home/mininet/Documents/own_code/rest_experiment5_' + networkupdate_number + '_initial.sh' )




#	startLoadGeneratorServerOnH2(pHost)
#	startLoadGeneratorClientOnH1(pHost)



    print("> start the LoadGeneratorServer on h2")
#    h2.cmd( 'java -jar /home/mininet/Downloads/LoadGeneratorServer.jar' )
    start_new_thread(startLoadGeneratorServerOnH2, (h2,))


    print("> start the LoadGeneratorClient on h1")
#    h1.cmd( 'java -jar -Xmx1024m /home/mininet/Downloads/LoadGeneratorClient.jar 10.0.0.2' )
    start_new_thread(startLoadGeneratorClientOnH1, (h1,))

    print("> wait 0.1 seconds")
    time.sleep( 0.1 )

    print("> start wireshark")        # str(sys.argv[1])
#    subprocess.call( '/home/mininet/start_wireshark2.sh ' + str(sys.argv[1]) )
#    thread.start_new_thread(startWireshark, ())
# args, bufsize=0, executable=None, stdin=None,stdout=None, stderr=None, preexec_fn=None, close_fds=False, shell=False, cwd=None, env=None, universal_newlines=False, startupinfo=None,creationflags=0
#    proc = subprocess.Popen(['bash','/home/mininet/start_wireshark2.sh',str(experiment_number),str(round_time),str(networkupdate_number)],stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, bufsize=0, close_fds=True)
#    proc = subprocess.Popen(['bash','/home/mininet/start_wireshark2.sh',str(experiment_number)],str(round_time),str(networkupdate_number),bufsize=1)

    proc = subprocess.Popen(['sudo', 'bash','/home/mininet/start_wireshark2.sh',str(experiment_number),str(round_time),str(networkupdate_number)],stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)

    print("> start of wireshark has been triggerd")


    errQ = Queue.Queue()
    outQ = Queue.Queue()
    Tout = Thread(target=enqueue_output, args=(proc.stdout, outQ))
    Terr = Thread(target=enqueue_error, args=(proc.stderr, errQ))
    Tout.daemon = True # thread dies with the program
    Terr.daemon = True # thread dies with the program
    Tout.start()
    Terr.start()

# ... do other things here

# read line without blocking


    line = ""
    while "Capturing" not in line:
#	current_time = datetime.datetime.now().time() 
#	print current_time.isoformat()
        try:
            line = outQ.get_nowait() # or q.get(timeout=.1)
        except Queue.Empty:
#	    print("nothing in out pipe")
            try:
	        line = errQ.get_nowait() # or q.get(timeout=.1)
            except Queue.Empty:
#		print("nothing in err pipe")
	        time.sleep(0.01)
	    else:
		print("> " + line)
        else:
            print("> " + line)
#        else: # got line

    print("> wireshark start capturing")

    print("> wait 1.5 seconds")
    time.sleep( 1.5 )

    print("> trigger network route update")
    os.system( '/home/mininet/Documents/own_code/rest_experiment5_' + networkupdate_number + '_own_shedule_random_delay.sh ' + round_time )

    print("> wait 10 seconds")
    time.sleep( 10 )


if __name__ == '__main__':
    setLogLevel( 'info' )
    myNet()
