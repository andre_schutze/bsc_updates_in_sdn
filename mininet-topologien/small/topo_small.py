#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController
from mininet.cli import CLI
from mininet.link import TCLink
from mininet.log import setLogLevel, info

def myNet():


    #OpenDayLight controller
#    ODL_CONTROLLER_IP='10.0.0.4'

    #Floodlight controller
#    FL_CONTROLLER_IP='10.0.0.5'

    net = Mininet( topo=None, build=False, link=TCLink)

    # Create nodes
    h1 = net.addHost( 'h1' )
    h2 = net.addHost( 'h2' )

    # Create switches
    s1 = net.addSwitch( 's1' )

    print "*** Creating links"
    net.addLink(h1, s1, port1=1, port2=1, bw=100 )
    net.addLink(s1, h2, port1=2, port2=1, bw=100 )
   

    # Add Controllers
    ryu_ctrl_rest = net.addController( 'c0', controller=RemoteController, ip='127.0.0.1', port=6633)

#    fl_ctrl = net.addController( 'c1', controller=RemoteController, ip=FL_CONTROLLER_IP, port=6633)


    net.build()

    # Connect each switch to the controller
    s1.start( [ryu_ctrl_rest] )

    s1.cmdPrint('ovs-vsctl show')

    CLI( net )
#    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNet()
