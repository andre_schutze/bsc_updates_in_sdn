#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController
from mininet.cli import CLI
from mininet.link import TCLink
from mininet.log import setLogLevel, info

def myNet():


    #OpenDayLight controller
#    ODL_CONTROLLER_IP='10.0.0.4'

    #Floodlight controller
#    FL_CONTROLLER_IP='10.0.0.5'

    net = Mininet( topo=None, build=False, link=TCLink)

    # Create nodes
    h1 = net.addHost( 'h1' )
#    h1.setMac("00:00:00:00:00:01", "eth0")
    h2 = net.addHost( 'h2' )
#    h2.setMac("00:00:00:00:01:01", "eth0")

    # Create switches
    s1 = net.addSwitch( 's1' )
    s2 = net.addSwitch( 's2' )
    s3 = net.addSwitch( 's3' )
    s4 = net.addSwitch( 's4' )
    s5 = net.addSwitch( 's5' )
    s6 = net.addSwitch( 's6' )
    s7 = net.addSwitch( 's7' )
    s8 = net.addSwitch( 's8' )
    s9 = net.addSwitch( 's9' )
    s10 = net.addSwitch( 's10' )
    s11 = net.addSwitch( 's11' )
    s12 = net.addSwitch( 's12' )
    s13 = net.addSwitch( 's13' )
    s14 = net.addSwitch( 's14' )
    s15 = net.addSwitch( 's15' )
    s16 = net.addSwitch( 's16' )
    s17 = net.addSwitch( 's17' )
    s18 = net.addSwitch( 's18' )
    s19 = net.addSwitch( 's19' )
    s20 = net.addSwitch( 's20' )
    s21 = net.addSwitch( 's21' )
    s22 = net.addSwitch( 's22' )
    s23 = net.addSwitch( 's23' )
    s24 = net.addSwitch( 's24' )

    print "*** Creating links"
    net.addLink(h1, s1, port1=1, port2=1, bw=10 )
    net.addLink(s24, h2, port1=1, port2=1, bw=10 )
    net.addLink(s1, s24, port1=4, port2=4, bw=10 )  


    net.addLink(s1, s2, port1=2, port2=1, bw=10 )  
    net.addLink(s2, s3, port1=2, port2=1, bw=10 )  
    net.addLink(s3, s4, port1=2, port2=1, bw=10 )  
    net.addLink(s4, s5, port1=2, port2=1, bw=10 )
    net.addLink(s5, s6, port1=2, port2=1, bw=10 )
    net.addLink(s6, s7, port1=2, port2=1, bw=10 )
    net.addLink(s7, s8, port1=2, port2=1, bw=10 )
    net.addLink(s8, s9, port1=2, port2=1, bw=10 )
    net.addLink(s9, s10, port1=2, port2=1, bw=10 )
    net.addLink(s10, s11, port1=2, port2=1, bw=10 )
    net.addLink(s11, s12, port1=2, port2=1, bw=10 )
    net.addLink(s12, s13, port1=2, port2=1, bw=10 )
    net.addLink(s13, s14, port1=2, port2=1, bw=10 )
    net.addLink(s14, s15, port1=2, port2=1, bw=10 )
    net.addLink(s15, s16, port1=2, port2=1, bw=10 )
    net.addLink(s16, s17, port1=2, port2=1, bw=10 )
    net.addLink(s17, s18, port1=2, port2=1, bw=10 )
    net.addLink(s18, s19, port1=2, port2=1, bw=10 )
    net.addLink(s19, s20, port1=2, port2=1, bw=10 )
    net.addLink(s20, s21, port1=2, port2=1, bw=10 )
    net.addLink(s21, s22, port1=2, port2=1, bw=10 )
    net.addLink(s22, s23, port1=2, port2=1, bw=10 )
    net.addLink(s23, s24, port1=2, port2=2, bw=10 )

    
    net.addLink(s1, s13, port1=3, port2=3, bw=10 )  
    net.addLink(s2, s14, port1=3, port2=3, bw=10 )  
    net.addLink(s3, s15, port1=3, port2=3, bw=10 )  
    net.addLink(s4, s16, port1=3, port2=3, bw=10 )  
    net.addLink(s5, s17, port1=3, port2=3, bw=10 )
    net.addLink(s6, s18, port1=3, port2=3, bw=10 )
    net.addLink(s7, s19, port1=3, port2=3, bw=10 )
    net.addLink(s8, s20, port1=3, port2=3, bw=10 )
    net.addLink(s9, s21, port1=3, port2=3, bw=10 )
    net.addLink(s10, s22, port1=3, port2=3, bw=10 )
    net.addLink(s11, s23, port1=3, port2=3, bw=10 )
    net.addLink(s12, s24, port1=3, port2=3, bw=10 )
  

    net.addLink(s13, s2, port1=4, port2=4, bw=10 )
    net.addLink(s14, s3, port1=4, port2=4, bw=10 )
    net.addLink(s15, s4, port1=4, port2=4, bw=10 )
    net.addLink(s16, s5, port1=4, port2=4, bw=10 )
    net.addLink(s17, s6, port1=4, port2=4, bw=10 )
    net.addLink(s18, s7, port1=4, port2=4, bw=10 )
    net.addLink(s19, s8, port1=4, port2=4, bw=10 )
    net.addLink(s20, s9, port1=4, port2=4, bw=10 )
    net.addLink(s21, s10, port1=4, port2=4, bw=10 )
    net.addLink(s22, s11, port1=4, port2=4, bw=10 )
    net.addLink(s23, s12, port1=4, port2=4, bw=10 )


    # Add Controllers
    ryu_ctrl_rest = net.addController( 'c0', controller=RemoteController, ip='127.0.0.1', port=6633)

#    fl_ctrl = net.addController( 'c1', controller=RemoteController, ip=FL_CONTROLLER_IP, port=6633)


    net.build()

    # Connect each switch to the controller
    s1.start( [ryu_ctrl_rest] )
    s2.start( [ryu_ctrl_rest] )
    s3.start( [ryu_ctrl_rest] )
    s4.start( [ryu_ctrl_rest] )
    s5.start( [ryu_ctrl_rest] )
    s6.start( [ryu_ctrl_rest] )
    s7.start( [ryu_ctrl_rest] )
    s8.start( [ryu_ctrl_rest] )
    s9.start( [ryu_ctrl_rest] )
    s10.start( [ryu_ctrl_rest] )
    s11.start( [ryu_ctrl_rest] )
    s12.start( [ryu_ctrl_rest] )
    s13.start( [ryu_ctrl_rest] )
    s14.start( [ryu_ctrl_rest] )
    s15.start( [ryu_ctrl_rest] )
    s16.start( [ryu_ctrl_rest] )
    s17.start( [ryu_ctrl_rest] )
    s18.start( [ryu_ctrl_rest] )
    s19.start( [ryu_ctrl_rest] )
    s20.start( [ryu_ctrl_rest] )
    s21.start( [ryu_ctrl_rest] )
    s22.start( [ryu_ctrl_rest] )
    s23.start( [ryu_ctrl_rest] )
    s24.start( [ryu_ctrl_rest] )


#    s1.cmdPrint('ovs-vsctl show')

    CLI( net )
#    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNet()
