\label{sec:consistent_update_algorithms}
This thesis gives insights in network route update algorithms which provide consistency properties. It presents the algorithms WayUp, which provide waypoint enforcement and Peacock, which provide weak/relaxed loop freedom.

\section{WayUp}
The network route update algorithm WayUp perform network route updates, which fulfill the consistency property of waypoint enforcement. We already show the advantage of a waypoint enforced network route update in \ref{sec:introduction}. Information about WayUp from a theoretical perspective and a proof, that the algorithm ensures waypoint enforcement can be found in \cite{Ludwig2014}.

\subsection{Explenation using an Example}
\label{subsec:explenation_of_wayup_using_an_example}

To network route updates belong an old and new route through the network, a node/switch which is set as waypoint and a time, the controller have to wait after round three to ensure, that there are no network packets on the old route between source and waypoint. The old route and the new route should contain the waypoint. The old route, the new route, the waypoint and the time to wait are input parameters for the WayUp algorithm. The algorithm WayUp updates nodes/switches in one to four rounds. The number of rounds the update takes, depends on the old route, the new route and the waypoint.

\begin{figure}
\centering
\includegraphics[width=7cm]{./topologien/Topologie_wayup.png}
\caption{old and new route through a network}
\label{fig:explenation_of_wayup_old_and_new_route}
\end{figure}

Figure \ref{fig:explenation_of_wayup_old_and_new_route} show a graph, which contain all the information needed to trigger a WayUp update (except the time to wait). The edges, which have a solid line, build the old route through the network. The edges, which have a dashed line, build the new route through the network. The node filled with black colour is the waypoint. The time the controller have to wait after round three does not affect the mapping of the nodes/switches to rounds, which WayUp calculate.

In the first round the algorithm update all nodes/switches, which are part of the new route, but which are not part of the old route. If you have a look at the figure you see, that there are no nodes/switches which are part of the new route, but which are not part of the old route. So in this example no nodes/switches are updated during round one.

In the second round the algorithm update all nodes/switches behind the waypoint of the old route, which lies in front of the waypoint on the new route and which have a backward rule. A backward rule is an edge of the new route in the graph, which destination lies in front of its source relative to the old route. Corresponding to this a forward rule can be defined. In the actual example the nodes/switches s3, s4, s5 and s6 lie behind the waypoint of the old route. Of these nodes/switches s3, s4 and s5 lie in front of the waypoint of the new route. Of these nodes/switches s4 and s5 have a backward rule. So during round two s4 and s5 are updated.

In the third round of the algorithm all nodes/switches are updated which lie in front of the waypoint of the new round and which are not updated in the second round. S1, s3, s4, s5 lie in front of the waypoint of the new route. S4 and s5 are already updated in round two. So in round three s1 and s3 are updated.

Between round tree and round four the algorithm have to wait. To guarantee waypoint enforcement the packets on the old route, which have not pass the waypoint have to pass it. After the waiting period round four starts. In this round the nodes/switches on the new route behind the waypoint can be updated. In the example these are the nodes/switches s2 and s6 \cite{Ludwig2014}.

\subsection{Optimization}
\label{subsec:wayup_optimization}
There are some cases, the WayUp algorithm uses too many update rounds to fulfill the consistency property of waypoint enforcement. This can be optimized.

Lets have a look at the case, the waypoint is set to the source of the route. In this case the WayUp algorithm would send OpenFlow messages to all switches during round four. If there are nodes/switches, which are part of the new route but which are not part of the old route the algorithm would also send OpenFlow messages during round one. If the waypoint is set to the source of the route all packets entering the route will pass the waypoint and also an update schedule consisting of one round without a time to wait would ensure waypoint enforcement.

The algorithm could also be optimized in the case the waypoint is set to the destination of the route. This case is similar to the previous one. All OpenFlow messages would be send in round three and some could be send during round one. In this case the time to wait would not affect the duration of the update because it does not matter what the controller do between round three and four if the update end after round three. But all packets leaving the route would pass the waypoint. An update schedule with one round would ensure waypoint enforcement.

There is another case of interest. To shorten the explanation we adopt the notation used in \cite{Ludwig2014}.

$\pi_{old}$ ... nodes/switches on the old route

$\pi_{new}$ ... nodes/switches on the new route

$\pi_{old}^{<wp}$ ... nodes/switches in front of the waypoint on the old route

$\pi_{old}^{>wp}$ ... nodes/switches behind the waypoint on the old route

$\pi_{new}^{<wp}$ ... nodes/switches in front of the waypoint on the new route

$\pi_{new}^{>wp}$ ... nodes/switches behind the waypoint on the new route

Imagine an update with no nodes/switches, which belong to $\pi_{new}^{<wp}$ and which belong to $\pi_{old}^{>wp}$ (1) and with no nodes/switches, which belong to $\pi_{old}^{<wp}$ and which belong to $\pi_{new}^{>wp}$ (2) and all nodes/switches, which belong to $\pi_{new}$ also belong to $\pi_{old}$ (3). In this case the WayUp algorithm would send OpenFlow messages in two rounds. The algorithm would update all nodes/switches, which belong to $\pi_{new}^{<wp}$ in round three and the algorithm would update all nodes/switches, which belong to $\pi_{new}^{>wp}$ in round four. 

In an update, which meet (1), (2) and (3) all nodes/switches, which belong to $\pi_{new}^{<wp}$ have to belong to $\pi_{old}^{<wp}$. If there would be a node/switch, which belong to $\pi_{new}^{<wp}$ and which do not belong to $\pi_{old}^{<wp}$ it could:

a) belong to $\pi_{old}$, but this node/switch could not exist because of (3)

b) belong to $\pi_{old}^{>wp}$, but this node/switch could not exist because of (1)

In the same way you can show that in an update, which meet (1), (2) and (3) all nodes/switches, which belong to $\pi_{new}^{>wp}$ also belong to $\pi_{old}^{>wp}$. That means that you do not have to consider waiting times and update rounds. During the network route update the packets entering the route will travel along a path in front of the waypoint, pass the waypoint and travel along a path behind the waypoint.

\section{Peacock}
Peacock is a network route update algorithm. It guarantees weak/relaxed loop freedom during the update. We already present the difference between strong loop freedom and weak/relaxed loop freedom and the advantages of weak/relaxed loop free network route updates in \ref{sec:introduction}.

%An example how a network route update can lead to looping packets is given in \ref{Differences_between_Flow_Table_Entries}.


\subsection{Explanation of the Algorithm}
This section should show how Peacock work and how the algorithm guarantee weak/relaxed loop freedom of network route updates. To guarantee relaxed loop freedom, Peacock uses two different strategies. The first one is named "Shortcut" phase. During this phase long forward edges are chosen. The chosen edges should not cross each other. That means the destination node/switch of one edge have to be in front of the source node/switch of another edge relative to the old route. The corresponding source nodes/switches to the edges are updated in the actual round. There is an example in figure \ref{fig:peacock_old_and_new_route}. In the graph, the edges from s1 to s5, s2 to s6, s4 to s9, s7 to s10 and s10 to 12 are forward edges. The longest forward edge has s4 as its source and s9 as its edge. Peacock take this edge and search other forward edges, which do not cross this edge. The algorithm find only the edge from s10 to s12. So in the first round s4 and s10 are updated. After these nodes/switches are updated the new route from source to destination is s1, s2, s3, s4, s9, s10, s12.

\begin{figure}
\centering
\includegraphics[width=10cm]{./topologien/Topologie_peacock.png}
\caption{old and new route of example}
\label{fig:peacock_old_and_new_route}
\end{figure}

The second strategy is called "Prune" phase. After the "Shortcut" phase there are nodes/switches which do not lie on the actual route from source to destination. This nodes/switches are updated during the "Prune" phase. To meet the consistency property of relaxed loop freedom only the packets newly enter the route should not face a loop. The route from source to destination has to be loop free. Nodes/switches, which do not lie on this route can easily be updated. After executing the first "Shortcut" phase the route from source to destination changes and the nodes s5, s6, s7, s8 and s11 do not lie on this route. These nodes can be updated during the "Prune" phase.

The "Shortcut" phase is executed in odd rounds of the algorithm (e.g. 1,3,5,...) and the "Prune" phase is executed in even rounds of the algorithm (e.g. 2,4,6,...). After every phase the nodes/switches, which were updated are merged. If the node/switch s4 is updated and s4 has an edge to s9 on the new route s4 and s9 are merged. In our example after merging s4 with s9 the route from source to destination is s1, s2, s3, [s4, s9], s10, s11, s12. Packets, which pass s4 before s4 and s9 are merged travel the route s5, s6, s7, s8, [s4, s9], s10, s11, s12 to the destination. Node merging shrinks the graph. If the graph only consists of one node the algorithm terminates \cite{Ludwig2015}.

\subsection{Example how a packet can loop during a Weak/Relaxed Loop Free update}
\label{sec:Peacock_example_loop}
The next paragraphs should provide an understanding of the circumstances, which lead to loops during weak/relaxed loop free network route updates. At first it is depicted, how Peacock perform a network route update. You will see, that the update meet weak/relaxed loop freedom. Then the circumstances are highlighted, which lead to loops during this update.

Updating a route through a computer network involves the update of different switches. To fulfill relaxed loop freedom an update is split in different rounds. A relaxed loop free route update guarantees that in every network state during the update, there are no loops between source and destination. That means that all network packets newly entering the route don't face a loop between source and destination \cite{Ludwig2015}. Lets  have a look at the update shown in figure \ref{fig:example_loop_routes}.

\begin{figure}
\centering
\includegraphics[width=6cm]{./topologien/Topologie_consistent_network_updates_peacock.png}
\caption{example network update}
\label{fig:example_loop_routes}
\end{figure}

To do the network update in a weak loop free manner the network update algorithm Peacock is used. Peacock do the update in three rounds. Node/switch s1 is updated in the first round, nodes/switches s2 and s3 are updated in the second round and node/switch s4 is updated in the third round. Lets focus on the path between source and destination. This path is important for network packets, which newly enter the route. Before the network route update the path from source to destination is s1, s2, s3, s4, s5. The packets travel along the solid line. After s1 is updated the network look like shown in figure \ref{fig:example_loop_routes_after_1}.

\begin{figure}
\centering
\includegraphics[width=6cm]{./topologien/Topologie_consistent_network_updates_peacock_after1.png}
\caption{example network update after round one}
\label{fig:example_loop_routes_after_1}
\end{figure}

Lets again have a look on the path between source and destination. Now network packets, which newly enter the route travel along the path s1, s4, s5. This path is loop free. If a network packet is situated for example in the memory of switch s2 it will take the route s2, s3, s4, s5. After the network update is processed by switch s1, the new forwarding rule is implemented in the forwarding table of switch s1 and the controller receives the acknowledgement of s1. Update round one finishes and update round two starts. In round two only nodes/switches are updated which do not lie on the path between source and destination. Nodes/switches s2 and s3 don't lie on the path. S2 and s3 can be updated without violating weak/relaxed loop freedom. After round two of the network update the network look like shown in figure \ref{fig:example_loop_routes_after_2}.

\begin{figure}
\centering
\includegraphics[width=6cm]{./topologien/Topologie_consistent_network_updates_peacock_after2.png}
\caption{example network update after round two}
\label{fig:example_loop_routes_after_2}
\end{figure}

After round two the path from source to destination is still s1, s4, s5. There are no loops for packets entering the route. If a network packet rest for example in the memory of switch s2 it will now take the path s2, s5. After all nodes/switches, which are updated in round two implement the new forwarding rules in their forwarding tables and the controller receives all acknowledgements, round two ends and round three starts. In round tree the last node/switch, s4 is updated. After this network update the network and the route look like it is shown in figure \ref{fig:example_loop_routes_after_3}.

\begin{figure}
\centering
\includegraphics[width=6cm]{./topologien/Topologie_consistent_network_updates_peacock_after3.png}
\caption{example network update after the whole network update finishes}
\label{fig:example_loop_routes_after_3}
\end{figure}

The path from source to destination is s1, s4, s3, s2, s5. Again there is no loop between source and destination. Like the above investigation show, for the weak loop free network update used in the example, there is no loop between source and destination for every network state during the update.

But during a weak/relaxed loop free update there are circumstances, packets can loop. Lets start from the network state like it is shown in figure \ref{fig:example_loop_routes_after_1} and imagine there is a network packet (p1) resting in the memory of switch s2. Lets also imagine that the controller processes the second round of the network route update and the controller already send out OpenFlow messages to the switches of round two. Lets also imagine that the OpenFlow message send to switch s3 reaches s3 earlier than the network update message send to switch s2 reaches s2. Switch s3 implement the new forwarding rule earlier than s2 does. Lets imagine that switch s3 has implemented the new forwarding rule and switch s2 has not. Time passes and switch s2 send out the network packet (p1) resting in its memory. According to the old forwarding rule s2 uses, s2 has to send out (p1) to s3. S3 already use the new forwarding rule and send out (p1) received from switch s2 out to s2. The network packet (p1) loop. Looping of the packet end when switch s2 also implement the new forwarding rule in its forwarding table like switch s3 does.

This example show how a packet can loop during a week/relaxed loop free network route update. With the network route update consistency property of strong loop freedom the depicted loop would not occur. But the number of packets which rest in the switches and links involved in the update are limited and therewith also the amount of packets which could loop during a weak/relaxed loop free update.
