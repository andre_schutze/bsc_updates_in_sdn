\label{sec:controller}
The algorithms presented and explained in \ref{sec:consistent_update_algorithms} are implemented in Ryu. "Ryu (is a) development framework, which is used to achieve Software Defined Networking" \cite{ryu_book} With the standard Ryu installation some Ryu apps are shipped. These apps provide different kinds of OpenFlow controllers. For this thesis the already existing app "ofctl\_rest.py" is used as basis for the implementation of a new Ryu app called "ofctl\_rest\_own.py". The Ryu app "ofctl\_rest.py" provide a REST - API and functionality for example to install flow entries in and retrieve information from OpenFlow switches. The app parses incoming REST messages and send OpenFlow messages to connected switches based on information retrieved from the REST messages. The original controller app provide for example functionality to do network updates consisting of a single OpenFlow message send from controller to switch. Functionality to do network route updates, which fulfill consistency properties were not implemented. For this thesis we implement the app "ofctl\_rest\_own.py", which is published in the repository (see \ref{sec:repository}). This app provide in addition to the original app four new resources, each connected with its own network route update. The resources have the following functionality:

\begin{itemize} 

	\item Sending a REST message to the  URL "http://\textless controller-address\textgreater:\textless controller-port\textgreater /stats/flowupdate" trigger the controller to perform a network route update using an update schedule calculated by the algorithm WayUp. 

	\item Sending a REST message to the  URL "http://\textless controller-address\textgreater:\textless controller-port\textgreater /stats/flowupdate\_peacock" trigger the controller to perform a network route update using an update schedule calculated by the algorithm Peacock. 

	\item Sending a REST message to the  URL "http://\textless controller-address\textgreater:\textless controller-port\textgreater /stats/flowupdate\_own\_shedule" trigger the controller to send a custom round based network update message. The user is able to specify in the REST message which node/switch the controller should update in which round.

	\item Sending a REST message to the  URL "http://\textless controller-address\textgreater:\textless controller-port\textgreater /stats/flowupdate\_own\_shedule\_random\_delay" trigger the controller to send a custom network route update message with random delay. This update work like the previous one, but in this one the user can specify the duration of an update round. The controller distribute the OpenFlow message of the actual round randomly over the time span of the round.

\end{itemize}

\section{Processing a REST request}
In the case a REST request reaches a resource, the controller parses the REST request at first. For every network route update a different REST message exist and a different parser is used. Then the controller create with the help of the information retrieved from the REST request a message object. The controller create one message object for every network route update REST request the controller receives. Every message object has a type of "OwnSheduleRandomDelayMessage", "OwnSheduleMessage", "WayUpMessage" or "PeacockMessage" depending on the resource the REST massage is received. During construction of the message object the controller calculate the update schedule. This schedule is calculated differently in every type of message. Every message contain a flag, which save the information whether it is currently processed or not. This flag is set to false by default. After the message is created it is enqueued in a message queue. The controller check if the first message of the queue is already in progress. If the first message of the queue is not in progress the message, which the controller enqueue is the only one in the queue. The controller start to process this message. All messages have in common that they save the update schedule and the OpenFlow messages in the message object and that every round of the update schedule is processed in the same way. If the controller start processing a message it start with the first round, which is set to the actual round. In the actual round a set of nodes/switches has to be updated. The controller retrieve the corresponding OpenFlow message for every node/switch in the set and sends them. Then the controller send a Barrier Request to every switch of the set and wait for Barrier Replies. For every Barrier Reply received the controller determine the source node/switch. This node/switch is removed from the set of nodes/switches of the actual round of the first message in the message queue. If the set is empty the actual round finishes and the controller can go on processing the next round of the message. If the message object doesn't have a next round the controller delete the message from the queue and start processing the next message.

The message queue is already mentioned in the previous paragraph. Network route update messages have to be processed sequentially. Every message rely on barrier requests. If one node/switch is affected by two different messages it could lead to problems in the case these two messages are processed in parallel. Imagine the following sequence of events: 

\begin{itemize} 

	\item The controller (c1) processes message (m1). It have to send an OpenFlow message (of1) to switch (s5).

	\item (c1) processes message (m2). It have to send on OpenFlow message (of2) to switch (s5).

	\item (c1) processes (m1). It send a Barrier Request (breq1) to (s5).

	\item (c1) processes (m2). It send a Barrier Request (breq2) to (s5).

	\item (of1) reaches (s5).

	\item (s5) apply (of1).

	\item (breq1) reaches (s5).

	\item (s5) sends Barrier Reply (brep1) to (c1).

	\item (of2) reaches (s5).

	\item (s5) implement (of2).

	\item (breq2) reaches (s5).

	\item (s5) sends Barrier Reply (brep2) to controller.

	\item (brep1) reaches (c1).

\end{itemize}

A Barrier Reply doesn't contain information, about the OpenFlow message or Barrier Request it acknowledges. (c1) does not know if (brep1) belong to (m1) or (m2). This example show, that it is not possible to process (m1) and (m2) in parallel. To solve this problem (m1) and (m2) have to be processed sequentially. To lower the complexity of implementation the controller doesn't analysed which messages can be processed in parallel and which have to be processed sequentially and therewith all messages are processed sequentially. To raise the performance of the controller this could be optimized. For the experiments done in this thesis it does not matter if messages are processed sequentially or in parallel.

Lets have a deeper look into the implementations of the network route update algorithms WayUp and Peacock.

\section{WayUp}
\subsection{Structure of the REST request}
Here is an example of a REST message:

\{

\hspace{0.5cm} "oldpath":[\textless dp-num\textgreater ,\textless dp-num\textgreater ,\textless dp-num\textgreater ,\textless dp-num\textgreater ],

\hspace{0.5cm} "newpath":[\textless dp-num\textgreater ,\textless dp-num\textgreater ,\textless dp-num\textgreater ,\textless dp-num\textgreater ],

\hspace{0.5cm} "wp":\textless dp-num\textgreater ,

\hspace{0.5cm} "interval":\textless time in ms\textgreater ,

\hspace{0.5cm} \textless type\textgreater :[\textless OpenFlow message information\textgreater ],

\hspace{0.5cm} \textless type\textgreater :[\textless OpenFlow message information\textgreater ],

\hspace{0.5cm} \textless type\textgreater :[\textless OpenFlow message information\textgreater ],

\hspace{0.5cm} \textless type\textgreater :[\textless OpenFlow message information\textgreater ],

\}

The WayUp REST request consist of a header part and a body part. The header part consists of the input parameters of WayUp. That are the old route, the new route, the waypoint and the time interval. In Ryu, the switches, which are connected to the controller are identified by integer values called datapaths. The waypoint is a string, which can be converted to an integer value and the old and new route is a string, which can be converted to a list of integer values. The integer values are ordered in the list like they are passed by the network packets along the route. That means if on the route at first node/switch 2 is passed, than node/switch 1 and than node/switch 3 the path would look like: [2, 1, 3].

In the body part of the REST message you can find information about the OpenFlow messages the controller have to send to the switches. In the common case one WayUp REST request consist of several OpenFlow messages. The OpenFlow messages have the structure like it is expected from the REST API of the controller. An OpenFlow message of body part could be send to "http://\textless controller-address\textgreater:\textless controller-port\textgreater /stats/flowentry/add" of the controller and the controller would add the flow entry to the switch specified in the flow entry. The type field is used to specify if the flow entry in the affected switch should be added to, updated or deleted from the flow table.

\subsection{Implementation}
Using the data from the REST request the algorithm calculate the schedule like we explain in the section \ref{subsec:explenation_of_wayup_using_an_example}. Section \ref{subsec:wayup_optimization} show, that there are special cases WayUp calculate schedules with to many rounds. The algorithm can be optimized. But in the controller app the original, unoptimized version is implemented. This is the one, which is described in \cite{Ludwig2014}.

For the implementation of WayUp the lists oldpath and newpath have to be scanned and the appropriate nodes/switches for the single rounds have to be extracted.

%It is important to mention that it is possible to write a REST request which keep information to update the route from host A to host B and from host B to host A. This update would change the bidirectional route from host A to host B and would end up in a waypoint enforced network state if the new route is waypoint enforced. The problem is that the WayUp algorithm ensures waypoint enforcement during an update only for one direction. To ensure waypoint enforcement during the update on both directions it is important to keep attention that you use one update REST request only for one direction. If you want to update the route also on the way back you have to issue a second REST request.

\section{Peacock}
\subsection{Structure of the REST request}
The REST request of the Peacock algorithm is similar to the REST request of the WayUp algorithm. It look like this:

\{

\hspace{0.5cm} "oldpath":[\textless dp-num\textgreater ,\textless dp-num\textgreater ,\textless dp-num\textgreater ,\textless dp-num\textgreater ],

\hspace{0.5cm} "newpath":[\textless dp-num\textgreater ,\textless dp-num\textgreater ,\textless dp-num\textgreater ,\textless dp-num\textgreater ],

\hspace{0.5cm} \textless type\textgreater :[\textless OpenFlow message information\textgreater ],

\hspace{0.5cm} \textless type\textgreater :[\textless OpenFlow message information\textgreater ],

\hspace{0.5cm} \textless type\textgreater :[\textless OpenFlow message information\textgreater ],

\hspace{0.5cm} \textless type\textgreater :[\textless OpenFlow message information\textgreater ],

\}

The REST request of Peacock is also divided in a header part and a body part, like the REST request of WayUp	. To the header information belong only the old path and the new path. The body part of the REST request contain all the OpenFlow messages needed to update the nodes/switches which are affected by the network route update. The body part of the Peacock REST request equal the body part of the WayUP REST request.

\subsection{Implementation}
When a REST request reaches the controller, the request is parsed and a "PeacockMessage" object is created with the information of the REST request. During construction of this object the update schedule is calculated using Peacock. To simplify the implementation of the calculation of the update schedule, some extra classes are implemented. The first one is an abstraction of the path. This class consists of a linked list, which contain node objects in the order they occur on the path. There are different methods to manipulate the list and the containing node objects. Merging of nodes/switches for example affect the route and the nodes/switches of the route. A node object can contain a single network node/switch, or in terms of node merging during the run of the algorithm, several nodes/switches. This abstraction makes it easier to implement the algorithm.
