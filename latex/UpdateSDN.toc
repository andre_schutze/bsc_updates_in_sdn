\contentsline {chapter}{\numberline {1}Abstract}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Abstract in German}{2}{chapter.2}
\contentsline {chapter}{\numberline {3}Introduction}{3}{chapter.3}
\contentsline {chapter}{\numberline {4}Related Work}{6}{chapter.4}
\contentsline {chapter}{\numberline {5}Consistent Update Algorithms}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}WayUp}{9}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Explenation using an Example}{9}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Optimization}{10}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Peacock}{11}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Explanation of the Algorithm}{12}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Example how a packet can loop during a Weak/Relaxed Loop Free update}{13}{subsection.5.2.2}
\contentsline {chapter}{\numberline {6}Implementation of a Consistent Update Framework}{16}{chapter.6}
\contentsline {section}{\numberline {6.1}Processing a REST request}{17}{section.6.1}
\contentsline {section}{\numberline {6.2}WayUp}{18}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Structure of the REST request}{18}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Implementation}{19}{subsection.6.2.2}
\contentsline {section}{\numberline {6.3}Peacock}{19}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Structure of the REST request}{19}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Implementation}{20}{subsection.6.3.2}
\contentsline {chapter}{\numberline {7}Methodology}{21}{chapter.7}
\contentsline {chapter}{\numberline {8}Empirical Evaluation}{23}{chapter.8}
\contentsline {section}{\numberline {8.1}Update consisting of one OpenFlow Message}{23}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}Measurement Setup}{23}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}Performing the Measurement}{23}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}Measurement Summary}{24}{subsection.8.1.3}
\contentsline {section}{\numberline {8.2}Duration of a Network Route Updates}{25}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Raw Execution Time of a Network Route Update}{25}{subsection.8.2.1}
\contentsline {subsubsection}{\numberline {8.2.1.1}Description of the Measurement}{25}{subsubsection.8.2.1.1}
\contentsline {subsubsection}{\numberline {8.2.1.2}Measurement Summary}{26}{subsubsection.8.2.1.2}
\contentsline {subsection}{\numberline {8.2.2}WayUp Update}{27}{subsection.8.2.2}
\contentsline {subsubsection}{\numberline {8.2.2.1}Description of the Submeasurements}{27}{subsubsection.8.2.2.1}
\contentsline {subsubsection}{\numberline {8.2.2.2}Measurement Summary}{27}{subsubsection.8.2.2.2}
\contentsline {subsection}{\numberline {8.2.3}Peacock Update}{32}{subsection.8.2.3}
\contentsline {subsubsection}{\numberline {8.2.3.1}Description of the Submeasurements}{32}{subsubsection.8.2.3.1}
\contentsline {subsubsection}{\numberline {8.2.3.2}Experiment Summary}{35}{subsubsection.8.2.3.2}
\contentsline {section}{\numberline {8.3}Loops in Relaxed Loop Free Updates}{37}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Measurement Setup}{37}{subsection.8.3.1}
\contentsline {subsubsection}{\numberline {8.3.1.1}Submeasurement 1}{38}{subsubsection.8.3.1.1}
\contentsline {subsubsection}{\numberline {8.3.1.2}Submeasurement 2}{40}{subsubsection.8.3.1.2}
\contentsline {subsubsection}{\numberline {8.3.1.3}Submeasurement 3}{40}{subsubsection.8.3.1.3}
\contentsline {subsubsection}{\numberline {8.3.1.4}Submeasurement 4}{46}{subsubsection.8.3.1.4}
\contentsline {subsection}{\numberline {8.3.2}Measurement Summary}{46}{subsection.8.3.2}
\contentsline {chapter}{\numberline {9}Conclusion}{47}{chapter.9}
\contentsline {chapter}{References}{49}{chapter*.3}
\contentsline {chapter}{\numberline {A}Installation}{51}{appendix.A}
\contentsline {section}{\numberline {A.1}How to start the Test Environment}{51}{section.A.1}
\contentsline {section}{\numberline {A.2}Repository}{51}{section.A.2}
