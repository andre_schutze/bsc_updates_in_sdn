#!/bin/bash


declare -a reference

while read line           
do           
    reference+=(`echo "$line"|tr -d '\n'`)
done <../networkupdate_exp2_01.txt


#declare -a wayup1
#
#while read line
#do
#    wayup1+=($line)
#done <../networkupdate_exp3_01.txt


declare -a wayup2

while read line
do
    wayup2+=($line)
done <../networkupdate_exp3_02.txt


declare -a wayup3

while read line
do
    wayup3+=($line)
done <../networkupdate_exp3_03.txt


declare -a wayup4

while read line
do
    wayup4+=($line)
done <../networkupdate_exp3_04.txt


#echo "reference;wayup1;wayup2;wayup3;wayup4" > data.dat
echo "reference;wayup2;wayup3;wayup4" > data.dat

#echo -n "-"
#echo -n ${reference[$i]}
#echo -n "-"
#exit 0

# reference wayup2 wayup3 wayup4
i=0

for i in `seq 0 2499`;
do
    echo -n ${reference[$i]} >> data.dat
    echo -n -e ";" >> data.dat
#    echo -n ${wayup1[$i]} >> data.dat
#    echo -n -e ";" >> data.dat
    echo -n ${wayup2[$i]} >> data.dat
    echo -n -e ";" >> data.dat
    echo -n ${wayup3[$i]} >> data.dat
    echo -n -e ";" >> data.dat
    echo ${wayup4[$i]} >> data.dat
done
