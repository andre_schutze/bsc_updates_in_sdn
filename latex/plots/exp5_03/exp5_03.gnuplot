set terminal png medium size 700,480
set samples 1001  # high quality
set border 31 linewidth .3 # thin border
set output "Topologie_experiment5_03_evaluation.png"
set decimalsign ','
set xlabel "round time"
set ylabel "looping packets"
plot "data200.dat" title 'exp5_03' with linespoints
