set samples 1000  # high quality
#set border 31 linewidth .3 # thin border
#set decimalsign ','


#set border 2 front lt black linewidth 1.000 dashtype solid
#set boxwidth 0.5 absolute
#set style fill   solid 0.25 border lt -1
#unset key
#set pointsize 0.5
#set style data boxplot
#set xtics border in scale 0,0 nomirror norotate  autojustify
#set xtics  norangelimit
#set xtics   ()
#set ytics border in scale 1,0.5 nomirror norotate  autojustify

#set ylabel "update duration in s" 
#set style boxplot candles range  1.50 outliers pt 7 separation 1 labels auto sorted
#x = 0.0



set title "Update Duration WayUp\n" 

set boxwidth  0.7 absolute
set pointsize 0.4
#
#unset key


set style fill solid 5.00 border -1
set style boxplot outliers pointtype 7
set style data boxplot


set xlabel "algorithm"
set xtics nomirror # don't draw scale on the top of the graph
set xtics ("reference" 1, "WayUp1" 2, "WayUp2" 3, "WayUp3" 4, "WayUp4" 5)

set ylabel "update duration"
set yrange [*:*]   # range of y axis set by input values
set ytics nomirror # don't draw scale on the right side of the graph

set terminal png size 1600,1200
set output "WayUp_durations.png" 

set datafile separator '\t'

plot 'data.dat' using (1):1 title 'reference',\
     'data.dat' using (2):2 title 'WayUp1', \
     'data.dat' using (3):3 title 'WayUp2', \
     'data.dat' using (4):4 title 'WayUp3', \
     'data.dat' using (5):5 title 'WayUp4'

