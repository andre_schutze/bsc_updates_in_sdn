#!/bin/bash


declare -a reference
#referenceI=0

while read line           
do           
    reference+=(`echo "$line"|tr -d '\n'`)
#    ((referenceI=referenceI+1))
done <../networkupdate_exp2_01.txt

#echo ${reference[*]}
#exit 0

declare -a wayup4
#wayup1I=0

while read line
do
    wayup4+=($line)
#    wayup1I=$wayup1I+1
done <../networkupdate_exp3_04.txt


declare -a peacock3
#wayup2I=0

while read line
do
    peacock3+=($line)
#    wayup2I=$wayup2I+1
done <../networkupdate_exp4_03.txt


declare -a peacock5
#wayup3I=0

while read line
do
    peacock5+=($line)
#    wayup3I=$wayup3I+1
done <../networkupdate_exp4_05.txt


echo "reference;wayup4;peacock3;peacock5" > data.dat

#echo -n "-"
#echo -n ${reference[$i]}
#echo -n "-"
#exit 0

# reference wayup1 wayup2 wayup3 wayup4
i=0

for i in `seq 0 2499`;
do
    echo -n ${reference[$i]} >> data.dat
    echo -n -e ";" >> data.dat
    echo -n ${wayup4[$i]} >> data.dat
    echo -n -e ";" >> data.dat
    echo -n ${peacock3[$i]} >> data.dat
    echo -n -e ";" >> data.dat
    echo ${peacock5[$i]} >> data.dat
done
